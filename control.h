/* \verbatim{control_h.tex} */

/* 
 * control.h
 * 
 * Header of fixative structures.
 */

#ifndef _CONTROL_H
#define _CONTROL_H

typedef struct
	{
	 int major;
	 char *jmp;
	 char *jz;
 	 struct cont1 *cnext; 
	 struct break1 *bnext;
	} WHILE1;

typedef struct
	{
	 int major;
	 char *jn;		/* Label of the JZ */
	 char *jmp2;		/* Label of the JMP. The first JMP */
				/* instruction. It is between expr2 */
				/* and expr3. */
	 char *jmp3;		/* Address where to jump if all */
				/* statements of the loop are */
				/* done. (See manual) */
	 struct break1 *bnext;
	 struct cont1 *cnext;
	} FOR1;

struct break1 
	{
	 char *adr;
	 struct break1 *next;
	};

struct cont1 
	{
	 char *adr;
	 struct cont1 *next;
	};

typedef struct
	{
	 int major;
	 char *jz;
	 char *jmp;
	} IF1;

struct default_usage
        {
	  int line_number;	/* Line number where the default label
				   was used. */
	  int def_flag;		/* Flag if the default label was used.
				   It can be used only once per switch
				   statement. (ANSI) */
	  char *adr;		/* Address where to jump to in the
				   virtual machine code. */
	};

typedef struct
        {
	  int major;
	  char *jz;
	  char *jmp;
	  struct default_usage def_use;
	  struct break1 *bnext;
	  struct list_const1 *next;
	} SWITCH1;

struct list_const1		/* The list of labels in switch
				   statement is created. The list is
				   at the end of switch statement
				   checked if labels in this list are
				   not duplicit. If there are duplicit
				   error message is issued. */
        {
	  int line_number;	/* Line number where the label was
				   used in the switch statement. */
	  int constant;		/* Label in switch statement. */
	  struct list_const1 *next;
	};

union fix
	{
	 WHILE1 while1;
	 FOR1 for1;
	 IF1 if1;
	 SWITCH1 switch1;
        };

/* \verbatim */

#endif /* _CONTROL_H */
