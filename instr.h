/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
/*
 * instr.h
 *
 * Header of structures.
 * Defines size of virtual machine
 * instructions.
 */

#ifndef _INSTR_H
#define _INSTR_H

/*
 * No operand, only major
 */
struct OPERAND_0_ma
{
  int major;
};

/*
 * No operand, only major and minor.
 */
struct OPERAND_0_mi
{
  int major;
  int minor;
};

/*
 * One operand and major only.
 */
struct OPERAND_1_ma
{
  int major;
  char *adr;
};	

/*
 * One operand and major and minor.
 */
struct OPERAND_1_mi
{
  int major;
  int minor;
  char *adr;
};

/*
 * One operand (immediately integer) and major and minor.
 */
struct OPERAND_1_i
{
  int major;
  int minor;
  int num;
};

/*
 * One operand (immediately unsigned integer) and major and minor.
 */
struct OPERAND_1_ui
{
  int major;
  int minor;
  unsigned int num;
};

/*
 * One operand (immediately long integer) and major and minor.
 */
struct OPERAND_1_li
{
  int major;
  int minor;
  long int num;
};

/*
 * One operand (immediately long unsigned integer) and major and minor.
 */
struct OPERAND_1_lui
{
  int major;
  int minor;
  long unsigned int num;
};

/*
 * One operand (immediately short integer) and major and minor.
 */
struct OPERAND_1_si
{
  int major;
  int minor;
  short int num;
};

/*
 * One operand (immediately short unsigned integer) and major and minor.
 */
struct OPERAND_1_sui
{
  int major;
  int minor;
  short unsigned int num;
};

/*
 * One operand (immediately double) and major and minor.
 */
struct OPERAND_1_id
{
  int major;
  int minor;
  double num;
};

/*
 * One operand (immediately long double) and major and minor.
 */
struct OPERAND_1_ild
{
  int major;
  int minor;
  long double num;
};

/*
 * One operand (immediately float) and major and minor.
 */
struct OPERAND_1_if
{
  int major;
  int minor;
  float num;
};

/*
 * One operand (immediately char) and major and minor.
 */
struct OPERAND_1_ic
{
  int major;
  int minor;
  char num;
};

/*
 * One operand (immediately signed char) and major and minor.
 */
struct OPERAND_1_isc
{
  int major;
  int minor;
  signed char num;
};

/*
 * One operand (immediately unsigned char) and major and minor.
 */
struct OPERAND_1_iuc
{
  int major;
  int minor;
  unsigned char num;
};

/*
 * Arithmetic and logical instructions.
 */
#define	 ADD  10
#define	 SUB  11
#define	 MULT 12
#define	 DIV  13
#define	 MOD  14
#define	 OR   15
#define	 AND  16
#define	 ORB   17
#define	 ANDB  18
#define	 EQ   19
#define	 LO   20 
#define	 GR   21
#define	 LE   22
#define	 GE   23
#define	 NE  24
#define  NEG 45
#define  NOT 46
#define  SAL 47
#define  SAR 48
#define  XOR 49
#define  CVT 44

/*
 * Control instructions.
 */
#define	 JMP  25
#define	 JNZ  26
#define  JZ   27 
#define	 RET  28 
#define	 CALLi 29
#define  HALT 34
#define  STOP 35 
#define  INTER 50
#define  IRET 51

/*
 * I/O instructions.
 */
#define	 OUT  30
#define	 IN   31 
#define  MESS 36

/*
 * Instructions on stacks.
 */
#define  PUSHAI 32
#define  PUSHA 33
#define  MOV 37
#define  PUSH 38
#define  POP 39
#define  POPA 40
#define  POPAD 41
#define  PUSHAD 42
#define  CLRT 43
#define  XCHG 52

#endif /* _INSTR_H */
