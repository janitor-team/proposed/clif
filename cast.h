/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
/*
 * cast.h
 *
 * defines for different type cast
 *
 */

#ifndef _CAST_H
#define _CAST_H

#define CS *(char **)		/* CONTENT OF STACK */
#define CCI *(int *)		/* CONTENT OF INTEGER CELL */
#define CCUI *(unsigned int *)	/* CONTENT OF UNSIGNED INTEGER CELL */
#define CCLI *(long int *)	/* CONTENT OF LONG INTEGER CELL */
#define CCSI *(short int *)	/* CONTENT OF SHORT INTEGER CELL */
#define CCLSI *(long signed int *) /* CONTENT OF LONG SIGNED INTEGER
				      CELL */
#define CCLUI *(long unsigned int *) /* CONTENT OF LONG UNSIGNED
					INTEGER CELL */ 
#define CCSUI *(short unsigned int *) /* CONTENT OF SHORT UNSIGNED
					 INTEGER CELL */ 
#define CCD *(double *)		/* CONTENT OF DOUBLE CELL */
#define CCLD *(long double *)	/* CONTENT OF LONG DOUBLE CELL */
#define CCF *(float *)		/* CONTENT OF FLOAT CELL */
#define CCC *(char *)		/* CONTENT OF CHAR CELL */
#define CCSC *(signed char *)	/* CONTENT OF SIGNED CHAR CELL */
#define CCUC *(unsigned char *)	/* CONTENT OF UNSIGNED CHAR CELL */
#define CCV *			/* CONTENT OF VOID CELL */
#define CFI (int (*)())		/* FUNCTION WITH INTEGER RETURN VALUE */
#define CFUI (unsigned int (*)()) /* FUNCTION WITH UNSIGNED INTEGER
				     RETURN VALUE */ 
#define CFLI (long int (*)())	/* FUNCTION WITH LONG INTEGER RETURN
				   VALUE */
#define CFLUI (long unsigned int (*)())	/* FUNCTION WITH LONG UNSIGNED
					   INTEGER RETURN VALUE */
#define CFSI (short int (*)())	/* FUNCTION WITH SHORT INTEGER RETURN
				   VALUE */
#define CFSUI (short unsigned int (*)()) /* FUNCTION WITH SHORT UNSIGNED
					    INTEGER RETURN VALUE */
#define CFD (double (*)())	/* FUNCTION WITH DOUBLE RETURN VALUE */
#define CFLD (long double (*)()) /* FUNCTION WITH LONG DOUBLE RETURN
				    VALUE */
#define CFF (float (*)())	/* FUNCTION WITH FLOAT RETURN VALUE */
#define CFC (char (*)())	/* FUNCTION WITH CHAR RETURN VALUE */
#define CFSC (signed char (*)()) /* FUNCTION WITH SIGNED CHAR RETURN
				    VALUE */
#define CFUC (unsigned char (*)()) /* FUNCTION WITH UNSIGNED CHAR
				      RETURN VALUE */
#define CFV (void (*)())	/* FUNCTION WITH VOID RETURN VALUE */

#endif /* _CAST_H */
