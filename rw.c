/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * rw.c
 *
 * This program is started when graphical window is opened.
 * It controls window events. If it is ExposureMask it redraws
 * whole window. It is suitable for window managers which these
 * events cannot process.
 */

#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>


XGCValues value;
XEvent event;

XWindowAttributes root_window_att;
XSetWindowAttributes mywin_att;

GC default_gc,set_gc;
Window root,mywin;
Pixmap mypix;
Display *disp;

int screen;
int width, height;
int x=0,y=0;
unsigned int width_w, height_w,border=3;
unsigned long bord=1L,back=0L;
char znak;

#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif

int main PROTO((int, char **, char**));

extern void mysigignore PROTO((void));

int
main (argc, argv, envp) 
  int argc;
  char **argv, **envp;
{
#if 0
  int cnt;
  printf (" starts in fork\n");
#endif

/*
 * Function for ignoring interrupts.
 */
  mysigignore ();


/* 
 * Parameters from parent process.
 */

  mywin = atol (argv[1]);  
  mypix = atol (argv[2]);

  if ((disp = XOpenDisplay (NULL)) == NULL)
    {
      printf (" XOpen fails \n");
      return (-1);
    }
#if 0
  XSynchronize (disp, 1);
#endif
  root = XDefaultRootWindow (disp);
  screen = XDefaultScreen (disp);
  if (!XGetWindowAttributes (disp, mywin, &root_window_att))
    {
      printf ("get window att failed \n");
      abort ();
    }


  width = XDisplayWidth (disp, screen);
  height = XDisplayHeight (disp, screen);
#if 0
  printf ("w: %d, h: %d \n", width, height);
#endif
  default_gc = XDefaultGC (disp, screen);

#if 0
  XSetState (disp, default_gc, 0L, 0L, GXcopy, 0xffffffffL);
  printf ("xsetstate in fork\n");
#endif


/* 
 * Event selection.
 */
 XSelectInput (disp, mywin, 
	       ExposureMask|StructureNotifyMask|SubstructureNotifyMask|KeyPressMask);
#if 0
 printf ("xselectinput in fork\n");
#endif
 while (1)
   {

/*
 * Processing of events.
 */
     XWindowEvent (disp, mywin,
		   ExposureMask|SubstructureNotifyMask|StructureNotifyMask,
		   &event);
#if 0
     printf ("cnt = %d\n", cnt);
     printf ("%d\n", event.type);
#endif
     if (event.type == 12)
       {
	 XCopyArea (disp, mypix, mywin, default_gc, 0, 0,
		    root_window_att.width, root_window_att.height, 0,
		    0);
#if 0
	 printf ("xcopyarea\n");
#endif
       }
     if (event.type == 17)
       {
#if 0
		printf("destroynotify\n");
#endif
		exit (0);
       }
     
   }
}



