/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * store_cont.c
 *
 * Stores and restores a context by context switching.
 */

#include <stdlib.h>

#include "global.h"
#include "mystdio.h"
#include "allocx.h"
#include "lex_t.h"
#include "input.h"
#include "store_cont.h"

#ifndef NOT_MSWIN_AND_YES_DOS

#ifdef FLEX_SCANNER
extern void *store_buffer_state ();
extern void flush_buffer ();
#endif

void
store_context ()
{
  struct CONTEXT *arch;
  if (NULL == (arch = (struct CONTEXT *)
	       callocx(1,sizeof(struct CONTEXT))))
    {
      error_message (4002);
      return;
    }
  arch->previous = context;
  context = arch;
  context->bp = bp;
  context->frame = frame;
  context->kodp = kodp;
  context->kodp1 = kodp1;
  context->kodp2 = kodp2;
  context->kodp3 = kodp3;
  context->pc = pc;
  context->stack = stack;
#ifdef FLEX_SCANNER
  context->state = store_buffer_state ();
#else
  context->input = input;
#endif
  context->tmp = tmp;
  context->tmph = tmph;
  tmph = tmp;
  kodp1 = kodp;
  kodp2 = kodp;
  kodp3 = NULL;
  kodp4 = NULL;
}

void
restore_context ()
{
  struct CONTEXT *arch;
  if (context != NULL)
    {
      if (kodp2 == kodp1)
	{
	  kodp = context->kodp;
	  kodp1 = context->kodp1;
	  kodp2 = context->kodp2;
	  kodp3 = context->kodp3;
	  kodp4 = context->kodp4;
	}
      arch = context;
      bp = context->bp;
      frame = context->frame;
      pc = context->pc;
      stack = context->stack;
      tmp = context->tmp;
      tmph = context->tmph;
#ifdef FLEX_SCANNER
      flush_buffer (context->state);
#else
      input = context->input;
#endif
      context = context->previous;
      free (arch);
    }
}

#endif
