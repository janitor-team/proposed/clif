#include <stdio.h>
#include <rpc/rpc.h>
#include "lin.h"

#define O_RDONLY 00
#define O_WRONLY 01

main(argc,argv)
     int argc;
     char **argv;
{
  CLIENT *cl;
  int *result;
  static int i,j;
  static remote_o_struct ros;
  static remote_r_w_struct rrws,*res;

  if(argc<3)
    {
      fprintf(stderr,"usage: host device\n");
      exit(0);
    }
  
  if((cl = clnt_create(argv[1],REMOTEDEVICE,REMOTEVERS,"tcp")) == NULL)
    {
      clnt_pcreateerror(argv[1]);
      exit(1);
    }
  
  ros.device=argv[2];
  ros.flags=O_WRONLY;
  if((result=remote_open_1(&ros,cl)) == NULL)
    {
      clnt_perror(cl,argv[1]);
      exit(1);
    }
  if(*result == -1)
    {
      printf("bad open\n");
      exit(1);
    }
  rrws.fd = *result;
  rrws.bytes = 2;

  for(i=0;i<5;i++)
    {
      for(j=0;j<4096;j++)
	{
	  *(short *)rrws.buf = (short)j;
	  
	  if((result=remote_write_1(&rrws,cl)) == NULL)
	    {
	      clnt_perror(cl,argv[1]);
	      exit(1);
	    }
	  if(*result == -1)
	    {
	      printf("bad write\n");
	      exit(1);
	    }
	}
      for(j=4095;j>=0;j--)
	{
	  *(short *)rrws.buf = (short)j;
 
	  if((result = remote_write_1(&rrws,cl)) == NULL)
	    {
	      clnt_perror(cl,argv[1]);
	      exit(1);
	    }
	  if(*result == -1)
	    {
	      printf("bad write\n");
	      exit(1);
	    }
	}
      }

  if((result=remote_close_1(&rrws.fd,cl)) == NULL)
    {
      clnt_perror(cl,argv[1]);
      exit(1);
    }
}
