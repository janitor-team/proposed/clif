typedef string devicetype<>;
typedef char cbuf[256];

struct remote_o_struct {
	devicetype device;
	int flags;
	int mode;
	};

struct remote_r_w_struct {
	int fd;
	cbuf buf;
	int bytes;
	};

program REMOTEDEVICE {
	version REMOTEVERS {
	int REMOTE_OPEN(remote_o_struct)=1;
	int REMOTE_CLOSE(int)=2;
	remote_r_w_struct REMOTE_READ(remote_r_w_struct)=3;
	int REMOTE_WRITE(remote_r_w_struct)=4;
	}=1;
}=99;
