/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * tables.h
 */

#ifndef _TABLES_H
#define _TABLES_H

#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif

extern int hastab_init PROTO((void));
extern int identtab_init PROTO((void));
extern int hastab_goto_init PROTO((void));

extern enum intern_arit_class integer_cons;
extern enum intern_arit_class doub_cons;
extern enum intern_arit_class flt_cons;
extern enum intern_arit_class chr_cons;
extern enum intern_arit_class vid_cons;

/* Type of an operand in an expression.  Length of an expression is
   limited to * 256 operands. */
extern enum intern_arit_class type_ac[]; 

extern int has PROTO((char *));
extern int point_call PROTO((char *));
extern int has_loc PROTO((int, char *));
extern int has_goto PROTO((char *));
extern int has_label PROTO((char *));
extern int has_tag PROTO((int, char *));
extern void fix_and_clear_goto_table PROTO((void));
extern void clear_hash_tab PROTO((void));
extern void clear_tag_tab PROTO((void));

/* Returns address of a global variable. */
extern struct ident_tab *point PROTO((char *));

/* Returns address of a local variable. */
extern struct ident_tab_loc *point_loc PROTO((char *));

extern void set_value PROTO((char *));

/* Adding information about type specifiers of formal parameters to
   the hash table. If it was done during declaration checking if it
   matches the definition. */
extern int add_spec_to_has PROTO((void));

/* Adding identifiers of formal parameters to the hash table. If it
   was done during declaration checking if it matches the definition. */
extern int add_ident_to_has PROTO((void));

/* Adding information about type specifiers of struct, union or enum
   fields to the hash table. */
extern int add_spec_to_tag PROTO((void));

/* Adding identifiers of struct, union or enum fields to the hash
   table. */
extern int add_ident_to_tag PROTO((void));

extern void link_function PROTO((int));

/* Clears hash table of local variables. */
extern void clear_hash_tab_declaration PROTO((void));
extern void init_zero PROTO((char *, unsigned));

extern int scope_offset_get PROTO((void));
extern int scope_offset_set PROTO((int));
extern int offset_aggregate_ident PROTO((void));

/* Adjust memory position for a type. */
extern void align_memory PROTO((char **, int));

extern void enter_file_scope PROTO((void));
extern void exit_file_scope PROTO((void));

extern int typedef_p PROTO((char *));
extern int get_declaration_line PROTO((char *));

extern int get_memory_size PROTO((int, struct internal_type *, int));
extern int get_field_size PROTO((int *, struct internal_type *));
extern void noninitialized_loc PROTO((char *));
#endif
