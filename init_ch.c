/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * init_ch.c
 *
 * Initialization of the channel compiler and 
 * redefinition of its input function.
 */
#include<ctype.h>

#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif

int input_channel PROTO((void));
void init_ch PROTO((char *));

int buf_cnt;
char *inp_buf;

/*
 * Redefinition of input for channel compiler.
 */
int
input_channel ()
{
  yy_channeltchar = yy_channelsptr>yy_channelsbuf?U_channel(*--yy_channelsptr):inp_buf[buf_cnt++];
  if (yy_channeltchar == 10)
    yy_channellineno++;
  if (yy_channeltchar == '\0')
    return (0);
  return (yy_channeltchar);
}

/*
 * Initialization of the channel compiler.
 */
void
init_ch (input_buffer)
  char *input_buffer;
{
  buf_cnt = 0;
  inp_buf = input_buffer;
}

/*
 * Redefinition of internal yacc function yywrap.
 */
int
yy_channelwrap ()
{
  return (1);
}
















