/*
 * sigig_lin.c
 *
 * Ignores interrupts in a fork-ed process.
 * It is used from rw.c file.
 */

#include <bsd/signal.h>


struct sigvec vec_fork, ovec_fork;

#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif

void mysigignore PROTO((void));

void 
mysigignore ()
{
  vec_fork.sv_handler = SIG_IGN;
  sigvec (SIGINT, &vec_fork, &ovec_fork);
}
