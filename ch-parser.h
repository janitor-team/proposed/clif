typedef union {
	int myint;
	double mydouble;
	char *mystring;
	} YY_channelSTYPE;
#define	NUMBERI	258
#define	NUMBERD	259
#define	STRING	260
#define	FIELDS	261
#define	TYPE	262
#define	PRINT_FORMAT	263
#define	DIRECTION	264
#define	START_TIME	265
#define	DURATION_TIME	266
#define	W_RESOLUTION	267
#define	LOWER	268
#define	UPPER	269
#define	STYLE	270
#define	AUTOMATIC	271
#define	ON_LEAVE_WINDOW	272


extern YY_channelSTYPE yy_channellval;
