/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * channel_maint.h
 * 
 * variables and structures for graphics channel
 *
 */

#ifndef _CHANNEL_MAINT_H
#define _CHANNEL_MAINT_H

#ifdef XWIN
#include <X11/Xlib.h>
#include <errno.h>
#endif

#ifdef MSWIN
#include <windows.h>
#endif




#define NUMBER_OF_CHANNELS 100
#define RECORD_WIDTH 100
#define DIMENSION 2
#define LINEAR_WINDOW_CONSTANT 2.


struct RECORD
{
  double x;
  struct RECORD *next;
};

struct LIST
{
  struct RECORD *head;
  struct RECORD *tail;
};

struct FIELD 
{
  double lower;
  double upper;
  double ax;
  double ay;
  double x_cur;
  double y_cur;
  unsigned long style;
#ifdef XWIN
  GC set_gc;
#endif
#ifdef MSWIN
  HPEN hpen, hpenp;
#endif
};

struct CHANNEL
{
  int fields;
  int cnt;
  double global_cnt;
  char *type;
  char *print_format;
  int direction;
  double start_time;
  int s_time;
  double duration_time;
  double d_time;
  int w_resolution[DIMENSION];
  char *on_leave_w;
  struct LIST *list;
  struct LIST *list_tmp;
  struct FIELD *member;
#ifdef XWIN
  Window mywin, mywin_write;
#ifdef FORK_YES 
  Pixmap mypix, mypix_write;
#endif
  XTextItem item;
  XFontStruct *myfont;
  int ch_pid, ch_pid_write;
#endif
#ifdef MSWIN
  HWND mywin,mywin_write;
  HDC mywinDC, mypixDC, mywin_writeDC, mypix_writeDC;
  char *item;
#endif
};

extern int channel_handle;
extern char window_name_init[];
extern char window_name_cur[];
extern char window_name_wr_init[];
extern char window_name_wr_cur[];
extern char window_name_tmp[];
extern struct CHANNEL channel[];

#endif /* _CHANNEL_MAINT_H */
