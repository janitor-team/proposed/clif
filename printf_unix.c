/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * printf_unix.c
 *
 * Redefinition of printf and fprintf functions.
 */

#include "printfx.h"

int
fprintfx VPROTO((FILE *stream, ...))
{
  va_list point;
#ifndef __STDC__
  FILE *stream;
#endif
  char *format;
  int tmp;
  
  VA_START (point, stream);

#ifndef __STDC__
  stream = va_arg (point, FILE *);
#endif
  format = va_arg (point, char *);
  tmp = vfprintf (stream, format, point);
  va_end (point);
  return tmp;
}

int 
printfx PVPROTO((char *format, ...))
{
  va_list point;
#ifndef __STDC__
  char *format;
#endif
  int tmp;
  
  VA_START (point, format);

#ifndef __STDC__
  format = va_arg (point, char *);
#endif
  tmp = vprintf (format, point);
  va_end (point);
  return tmp;
}
