/*  -*-c-*-
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * run_str_y
 *
 * grammar for run string parsing (yacc specification)
 */
%union {
	int myyint;
	char *myystring;
	}
%token <myyint> NUMBER 		/* integer constant */
%token <myystring> STRING
%token BGC HELP 
%token COMPILE WARNING
%token WARRANTY COPYING
%token VERBOSE VERSION
%token '='
%token WARNING_INHIBIT
%token WARNING_COMMENT WARNING_FORMAT WARNING_IMPLICIT WARNING_RETURN_TYPE
%token WARNING_TRIGRAPHS
%token WARNING_UNUSED WARNING_UNINITIALIZED
%token WARNING_EXTRA WARNING_AGGREGATE_RETURN
%token WARNING_ERROR
%token HANDLE_MAIN
%token CALL_BY_REFERENCE CALL_BY_VALUE
%token NO_CALL_BY_REFERENCE NO_CALL_BY_VALUE
%token DUMP_YACC DBG_INFO

%start list_stat_0

%{
#include <stdio.h>
#include "flags.h"
int argc_counter = 0;
extern int bc;
char *argvv[100];

#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif
extern int help PROTO((int));
int yylex PROTO((void));
void error_message PROTO((int));
int yyerror PROTO((char *));

int 
yyerror (s)
  char *s;
{
  return (0);
}

%}
%% /* beginnig of rules section */

list_stat_0 : list_stat_0  stat_0

	    |
	    ;

stat_0 : BGC '=' NUMBER
		{bc = $3;}

	| CALL_BY_REFERENCE
		{call_by_reference = 1;}
	| NO_CALL_BY_REFERENCE
		{call_by_reference = 0;}
	| CALL_BY_VALUE
		{call_by_value = 1;}
	| NO_CALL_BY_VALUE
		{call_by_value = 0;}
	| COMPILE
		{no_compile_only = 0;}

	| COPYING
		{
		  if (help (2) == -1)
		    return (-1);
		}
	| DUMP_YACC
		{
		  extern void dump_yacc PROTO((void));
		  dump_yacc ();
		}

	| DBG_INFO
		{dbg_symbols = 1;}

	| HANDLE_MAIN
		{handle_main = 1;}

	| HELP
		{
		  if (help (1) == -1)
		    return (-1);
	       	}

	| STRING
		{argvv[++argc_counter] = $1;}

	| VERBOSE
		{
		  if (help (4) == -1)
		    return (-1);
		}

	| VERSION
		{
		  if (help (5) == -1)
		    return (-1);
		}

	| WARNING
		{warning_yes = 1;}

	| WARNING_AGGREGATE_RETURN
		{warning_aggregate_return = 1;}

	| WARNING_COMMENT
		{warning_comment = 1;}

	| WARNING_ERROR
		{warnings_are_errors = 1;}

	| WARNING_EXTRA
		{warning_extra = 1;}

	| WARNING_FORMAT
		{warning_format = 1;}

	| WARNING_IMPLICIT
		{warning_implicit = 1;}

	| WARNING_INHIBIT
		{warning_inhibit = 1;}

	| WARNING_RETURN_TYPE
		{warning_return_type = 1;}

	| WARNING_TRIGRAPHS
		{warning_trigraphs = 1;}

	| WARNING_UNINITIALIZED
		{warning_uninitialized = 1;}

	| WARNING_UNUSED
		{warning_unused = 1;}

	| WARRANTY
		{
		  if (help (3) == -1)
		    return (-1);
		}

	| error
		{
		  error_message (7000); return (-1);
		}
       ;

%%
