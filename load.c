/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * load.c
 *
 * Function load for a dynamic loading of intrinsic functions.
 */

#include "mystdio.h" 
#include "load.h"

#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif

extern void link_function PROTO((int));
extern char *string PROTO((char *));

static int load_flag=0;

/*
 * Loads intrinsic C functions into the hash table.
 */
int
load (x)
  struct remote_tab **x;
{
  int i = 0;
  struct remote_tab *arch=NULL;
  if (!load_flag)
    {
      if (SIZE_REM > 0)
	{
	  *x = (struct remote_tab *) 
	    allocate (sizeof(struct remote_tab), PERM);
	  (*x)->name = intr_name[i].name;
	  (*x)->adr = (INTRINSIC_FUNCTION)intr_name[i].adr;
	  (*x)->next = NULL;
	  arch = *x;
	  for (i = 1; i < SIZE_REM; i++)
	    {
	      arch->next = (struct remote_tab *) 
		allocate (sizeof(struct remote_tab), PERM);
	      arch = arch->next;
	      arch->name = intr_name[i].name;
	      arch->adr = (INTRINSIC_FUNCTION)intr_name[i].adr;
	      arch->next = NULL;
	    }
	  load_flag = 1;
	  return (0);
	}
      else
	{
	  error_message (1008);
	  return (-1);
	}
    }
  else
    {
      return (1);
    }
}


static int load_flag_new=0;

int
load_new ()
{
  int i;
  
  if(!load_flag_new)
    {
      for(i = 0; i < SIZE_REM; i++)
	{
	  text = string (intr_name[i].name);
	  link_function (i);
	}
      load_flag_new = 1;
      return (0);
    }
  else
    return (load_flag_new);
}

int
unload ()
{
  load_flag_new = 0;
  return (0);
}
