/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

/*
 * global.c
 *
 * Definition of global variables.
 */

#include "type.h"		/* Needed for definition of constant */
				/* cells. */
#include "buf.h"		/* Needed for line_buf[YYLMAX]. */

int s;				/* Number of opened files. */ 
int char_counter;		/* Counter of characters. */
char line_buf[YYLMAX];		/* Contains whole line during lexical */
				/* analysis and compiling. */
char *mem_start;               /* Pointer to the memory. */
char *stack;                   /* Stack pointer */
char *bp;  	               /* Base pointer.
				* Used for relative addressing of variables.
				*/
char *frame;		       /* Frame pointer.
				* Used for parameter passing to 
				* remote functions.
				*/
char **ast;                    /* Arithmetic stack pointer. */
char *pc;                      /* Program counter.
				* Points to the next executed instruction.
				*/
char *fixst;    	       /* Pointer to the fixative stack */
#if 0
char **adr_stack;   	       /* Pointer to the address stack (obsolete). */
#endif
char *tmp;		       /* Pointer to the stack of temporaries. */
char *tmph;		       /* Pointer to the current level beginning
				* of the stack of temporaries.
				* It is used for clearing the stack.
				*/
char *tmp_start;	       /* Pointer to the beginning of the stack
				* of temporaries.
				*/
char *proc_name[256];	       /* Address of function beginning. */
char *proc_name_text[256];     /* Pointer to names of remote functions. */
char *struct_union_enum_name[256]; /* Names of tags. */

struct CONTEXT *context;
int proc;		       /* Counter for function call.
				* Used by nested calls when function is
				* used as parameter.
				*/
int suen_count;			/* struct_union_enum_name
				   counter. Nesting levels of tag
				   declaration. */

struct internal_type 
*type_com[256];			/* Complex type of an operand in
				 * expression (simple, array). */

int dim[256];		      /* Max size of array dimensions (256). */
int poc;		      /* Counter of the number of dimensions 
			       * in arrays.
			       */
int remote_flag = 0;	      /* Flag for remote function call. */
int body_flag = 1;	      /* Flag of a variable body. */
int typedef_f = 1;	      /* If in the context can be TYPENAME
				 token equals 1; 0 otherwise. */
int enum_value = 0;	      /* Value of the enumeration specifier. */

struct internal_type *typeh[256];
int type_spec_count = 0;

/* \verbatim{global_c_remote_ptr_C.tex} */

struct remote_tab 
*remote_ptr_C;                /* Pointer to the structure of remote 
			       * function table.
			       */

/* \verbatim */

/* \verbatim{global_c_rp.tex} */

struct return1 *rp;		/* Pointer for backpatch address of */
				/* return in a function. */

/* \verbatim */

char *kodp;			/* Pointer to the generated code. */
char *kodp1;			/* Pointer for sweeping out unuseful */
				/* code from the memory. */
char *kodp2;			/* Pointer for remembering context */
				/* used during context switching in */
				/* interrupts. */
char *kodp3;			/* Pointer for fixing returns in */
				/* functions. */
char *kodp4;			/* If there is l_value throw away last
				   PUSH instruction. */

/* Pointers to the last and current generated instruction. */
char *last_instr, *current_instr;

int count[256],count_arr=1;
int virtual_machine_suspended=1;
int clif_interrupt_level=0;

int set=0;

/* \verbatim{global_c_fixp.tex} */

union fix *fixp;              /* Pointer to the fixative stack. */

/* \verbatim */

char *call_fix[256];	      /* Address' backpatching during
			       * parameter passing.  */
