#ifndef _S_CONV_H_
#define _S_CONV_H_

#define PRINTF_P (!strcmp (proc_name_text[proc], "printf") || \
		  !strcmp (proc_name_text[proc], "fprintf"))

#define SCANF_P (!strcmp (proc_name_text[proc], "scanf") || \
		 !strcmp (proc_name_text[proc], "fscanf"))

extern int num_args[];		/* Number of arguments for function
				   (extern or local). */
extern int args[];		/* Counter for number of arguments for
				   function (extern or local). Used
				   when parsing a call. */
typedef struct internal_type *FORMAT_ARGS;

extern FORMAT_ARGS format_args[][256];

extern void compare_format_args PROTO((struct internal_type *,
				       struct internal_type *,
				       enum intern_arit_class));

#endif
