
/*  A Bison parser, made from channel.y with Bison version GNU Bison version 1.22
  */

#define YY_channelBISON 1  /* Identify Bison output_channel.  */

#define	NUMBERI	258
#define	NUMBERD	259
#define	STRING	260
#define	FIELDS	261
#define	TYPE	262
#define	PRINT_FORMAT	263
#define	DIRECTION	264
#define	START_TIME	265
#define	DURATION_TIME	266
#define	W_RESOLUTION	267
#define	LOWER	268
#define	UPPER	269
#define	STYLE	270
#define	AUTOMATIC	271
#define	ON_LEAVE_WINDOW	272


#line 26 "channel.y"
typedef union {
	int myint;
	double mydouble;
	char *mystring;
	} YY_channelSTYPE;
#line 40 "channel.y"

#include "global.h"
#include "mystdio.h"
#include "channel_maint.h"
#include "allocx.h"
#include "printfx.h"

int yy_channellex PROTO((void));
char *string PROTO((char *));
int yy_channelerror PROTO((char *));

int yy_channelerror(s)
    char *s;
{
   return(0);;
}

#ifndef YY_channelLTYPE
typedef
  struct yy_channelltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yy_channelltype;

#define YY_channelLTYPE yy_channelltype
#endif

#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YY_channelFINAL		52
#define	YY_channelFLAG		-32768
#define	YY_channelNTBASE	21

#define YY_channelTRANSLATE(x) ((unsigned)(x) <= 272 ? yy_channeltranslate[x] : 25)

static const char yy_channeltranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,    19,
    20,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
    18,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17
};

#if YY_channelDEBUG != 0
static const short yy_channelprhs[] = {     0,
     0,     3,     4,     8,    12,    16,    20,    24,    28,    32,
    37,    44,    51,    58,    60,    62,    64,    66
};

static const short yy_channelrhs[] = {    21,
    22,     0,     0,     6,    18,     3,     0,     7,    18,     5,
     0,     8,    18,     5,     0,    17,    18,     5,     0,     9,
    18,     5,     0,    10,    18,    24,     0,    11,    18,    23,
     0,    12,    18,     3,     3,     0,    13,    19,     3,    20,
    18,     4,     0,    14,    19,     3,    20,    18,     4,     0,
    15,    19,     3,    20,    18,     3,     0,     1,     0,     4,
     0,    16,     0,     4,     0,    16,     0
};

#endif

#if YY_channelDEBUG != 0
static const short yy_channelrline[] = { 0,
    59,    61,    64,    98,   105,   112,   119,   133,   135,   137,
   140,   149,   158,   167,   172,   175,   180,   183
};

static const char * const yy_channeltname[] = {   "$","error","$illegal.","NUMBERI",
"NUMBERD","STRING","FIELDS","TYPE","PRINT_FORMAT","DIRECTION","START_TIME","DURATION_TIME",
"W_RESOLUTION","LOWER","UPPER","STYLE","AUTOMATIC","ON_LEAVE_WINDOW","'='","'('",
"')'","list_stat_0","stat_0","d_time","s_time",""
};
#endif

static const short yy_channelr1[] = {     0,
    21,    21,    22,    22,    22,    22,    22,    22,    22,    22,
    22,    22,    22,    22,    23,    23,    24,    24
};

static const short yy_channelr2[] = {     0,
     2,     0,     3,     3,     3,     3,     3,     3,     3,     4,
     6,     6,     6,     1,     1,     1,     1,     1
};

static const short yy_channeldefact[] = {     2,
     0,    14,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     1,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     3,     4,     5,     7,    17,
    18,     8,    15,    16,     9,     0,     0,     0,     0,     6,
    10,     0,     0,     0,     0,     0,     0,    11,    12,    13,
     0,     0
};

static const short yy_channeldefgoto[] = {     1,
    14,    35,    32
};

static const short yy_channelpact[] = {-32768,
     0,-32768,   -16,   -15,   -14,   -13,     1,     2,     3,     4,
     5,     6,     8,-32768,    19,    22,    24,    26,    12,    14,
    29,    30,    31,    32,    33,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,    34,    16,    20,    21,-32768,
-32768,    25,    27,    28,    35,    38,    41,-32768,-32768,-32768,
    47,-32768
};

static const short yy_channelpgoto[] = {-32768,
-32768,-32768,-32768
};


#define	YY_channelLAST		47


static const short yy_channeltable[] = {    51,
     2,    15,    16,    17,    18,     3,     4,     5,     6,     7,
     8,     9,    10,    11,    12,    30,    13,    33,    19,    20,
    21,    26,    22,    23,    24,    25,    27,    31,    28,    34,
    29,    36,    37,    38,    39,    42,    41,    40,    48,    43,
    44,    49,    45,    50,    46,    47,    52
};

static const short yy_channelcheck[] = {     0,
     1,    18,    18,    18,    18,     6,     7,     8,     9,    10,
    11,    12,    13,    14,    15,     4,    17,     4,    18,    18,
    18,     3,    19,    19,    19,    18,     5,    16,     5,    16,
     5,     3,     3,     3,     3,    20,     3,     5,     4,    20,
    20,     4,    18,     3,    18,    18,     0
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/usr/local/gnu/lib/bison.simple"

/* Skeleton output_channel parser for bison,
   Copyright (C) 1984, 1989, 1990 Bob Corbett and Richard Stallman

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 1, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */


#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi)
#include <alloca.h>
#else /* not sparc */
#if defined (MSDOS) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
 #pragma alloca
#else /* not MSDOS, __TURBOC__, or _AIX */
#ifdef __hpux
#ifdef __cplusplus
extern "C" {
void *alloca (unsigned int);
};
#else /* not __cplusplus */
void *alloca ();
#endif /* not __cplusplus */
#endif /* __hpux */
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif /* alloca not defined.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yy_channelerrok		(yy_channelerrstatus = 0)
#define yy_channelclearin	(yy_channelchar = YY_channelEMPTY)
#define YY_channelEMPTY		-2
#define YY_channelEOF		0
#define YY_channelACCEPT	return(0)
#define YY_channelABORT 	return(1)
#define YY_channelERROR		goto yy_channelerrlab1
/* Like YY_channelERROR except do call yy_channelerror.
   This remains here temporarily to ease the
   transition to the new meaning of YY_channelERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YY_channelFAIL		goto yy_channelerrlab
#define YY_channelRECOVERING()  (!!yy_channelerrstatus)
#define YY_channelBACKUP(token, value) \
do								\
  if (yy_channelchar == YY_channelEMPTY && yy_channellen == 1)				\
    { yy_channelchar = (token), yy_channellval = (value);			\
      yy_channelchar1 = YY_channelTRANSLATE (yy_channelchar);				\
      YY_channelPOPSTACK;						\
      goto yy_channelbackup;						\
    }								\
  else								\
    { yy_channelerror ("syntax error: cannot back up"); YY_channelERROR; }	\
while (0)

#define YY_channelTERROR	1
#define YY_channelERRCODE	256

#ifndef YY_channelPURE
#define YY_channelLEX		yy_channellex()
#endif

#ifdef YY_channelPURE
#ifdef YY_channelLSP_NEEDED
#define YY_channelLEX		yy_channellex(&yy_channellval, &yy_channellloc)
#else
#define YY_channelLEX		yy_channellex(&yy_channellval)
#endif
#endif

/* If nonreentrant, generate the variables here */

#ifndef YY_channelPURE

int	yy_channelchar;			/*  the lookahead symbol		*/
YY_channelSTYPE	yy_channellval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YY_channelLSP_NEEDED
YY_channelLTYPE yy_channellloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yy_channelnerrs;			/*  number of parse errors so far       */
#endif  /* not YY_channelPURE */

#if YY_channelDEBUG != 0
int yy_channeldebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YY_channelINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YY_channelINITDEPTH
#define YY_channelINITDEPTH 200
#endif

/*  YY_channelMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YY_channelMAXDEPTH == 0
#undef YY_channelMAXDEPTH
#endif

#ifndef YY_channelMAXDEPTH
#define YY_channelMAXDEPTH 10000
#endif

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
int yy_channelparse (void);
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_channel_bcopy(FROM,TO,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_channel_bcopy (from, to, count)
     char *from;
     char *to;
     int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_channel_bcopy (char *from, char *to, int count)
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 184 "/usr/local/gnu/lib/bison.simple"
int
yy_channelparse()
{
  register int yy_channelstate;
  register int yy_channeln;
  register short *yy_channelssp;
  register YY_channelSTYPE *yy_channelvsp;
  int yy_channelerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yy_channelchar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yy_channelssa[YY_channelINITDEPTH];	/*  the state stack			*/
  YY_channelSTYPE yy_channelvsa[YY_channelINITDEPTH];	/*  the semantic value stack		*/

  short *yy_channelss = yy_channelssa;		/*  refer to the stacks thru separate pointers */
  YY_channelSTYPE *yy_channelvs = yy_channelvsa;	/*  to allow yy_channeloverflow to reallocate them elsewhere */

#ifdef YY_channelLSP_NEEDED
  YY_channelLTYPE yy_channellsa[YY_channelINITDEPTH];	/*  the location stack			*/
  YY_channelLTYPE *yy_channells = yy_channellsa;
  YY_channelLTYPE *yy_channellsp;

#define YY_channelPOPSTACK   (yy_channelvsp--, yy_channelssp--, yy_channellsp--)
#else
#define YY_channelPOPSTACK   (yy_channelvsp--, yy_channelssp--)
#endif

  int yy_channelstacksize = YY_channelINITDEPTH;

#ifdef YY_channelPURE
  int yy_channelchar;
  YY_channelSTYPE yy_channellval;
  int yy_channelnerrs;
#ifdef YY_channelLSP_NEEDED
  YY_channelLTYPE yy_channellloc;
#endif
#endif

  YY_channelSTYPE yy_channelval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yy_channellen;

#if YY_channelDEBUG != 0
  if (yy_channeldebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yy_channelstate = 0;
  yy_channelerrstatus = 0;
  yy_channelnerrs = 0;
  yy_channelchar = YY_channelEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yy_channelssp = yy_channelss - 1;
  yy_channelvsp = yy_channelvs;
#ifdef YY_channelLSP_NEEDED
  yy_channellsp = yy_channells;
#endif

/* Push a new state, which is found in  yy_channelstate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yy_channelnewstate:

  *++yy_channelssp = yy_channelstate;

  if (yy_channelssp >= yy_channelss + yy_channelstacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YY_channelSTYPE *yy_channelvs1 = yy_channelvs;
      short *yy_channelss1 = yy_channelss;
#ifdef YY_channelLSP_NEEDED
      YY_channelLTYPE *yy_channells1 = yy_channells;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yy_channelssp - yy_channelss + 1;

#ifdef yy_channeloverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YY_channelLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yy_channeloverflow is a macro.  */
      yy_channeloverflow("parser stack overflow",
		 &yy_channelss1, size * sizeof (*yy_channelssp),
		 &yy_channelvs1, size * sizeof (*yy_channelvsp),
		 &yy_channells1, size * sizeof (*yy_channellsp),
		 &yy_channelstacksize);
#else
      yy_channeloverflow("parser stack overflow",
		 &yy_channelss1, size * sizeof (*yy_channelssp),
		 &yy_channelvs1, size * sizeof (*yy_channelvsp),
		 &yy_channelstacksize);
#endif

      yy_channelss = yy_channelss1; yy_channelvs = yy_channelvs1;
#ifdef YY_channelLSP_NEEDED
      yy_channells = yy_channells1;
#endif
#else /* no yy_channeloverflow */
      /* Extend the stack our own way.  */
      if (yy_channelstacksize >= YY_channelMAXDEPTH)
	{
	  yy_channelerror("parser stack overflow");
	  return 2;
	}
      yy_channelstacksize *= 2;
      if (yy_channelstacksize > YY_channelMAXDEPTH)
	yy_channelstacksize = YY_channelMAXDEPTH;
      yy_channelss = (short *) alloca (yy_channelstacksize * sizeof (*yy_channelssp));
      __yy_channel_bcopy ((char *)yy_channelss1, (char *)yy_channelss, size * sizeof (*yy_channelssp));
      yy_channelvs = (YY_channelSTYPE *) alloca (yy_channelstacksize * sizeof (*yy_channelvsp));
      __yy_channel_bcopy ((char *)yy_channelvs1, (char *)yy_channelvs, size * sizeof (*yy_channelvsp));
#ifdef YY_channelLSP_NEEDED
      yy_channells = (YY_channelLTYPE *) alloca (yy_channelstacksize * sizeof (*yy_channellsp));
      __yy_channel_bcopy ((char *)yy_channells1, (char *)yy_channells, size * sizeof (*yy_channellsp));
#endif
#endif /* no yy_channeloverflow */

      yy_channelssp = yy_channelss + size - 1;
      yy_channelvsp = yy_channelvs + size - 1;
#ifdef YY_channelLSP_NEEDED
      yy_channellsp = yy_channells + size - 1;
#endif

#if YY_channelDEBUG != 0
      if (yy_channeldebug)
	fprintf(stderr, "Stack size increased to %d\n", yy_channelstacksize);
#endif

      if (yy_channelssp >= yy_channelss + yy_channelstacksize - 1)
	YY_channelABORT;
    }

#if YY_channelDEBUG != 0
  if (yy_channeldebug)
    fprintf(stderr, "Entering state %d\n", yy_channelstate);
#endif

  goto yy_channelbackup;
 yy_channelbackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yy_channelresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yy_channeln = yy_channelpact[yy_channelstate];
  if (yy_channeln == YY_channelFLAG)
    goto yy_channeldefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yy_channelchar is either YY_channelEMPTY or YY_channelEOF
     or a valid token in external form.  */

  if (yy_channelchar == YY_channelEMPTY)
    {
#if YY_channelDEBUG != 0
      if (yy_channeldebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yy_channelchar = YY_channelLEX;
    }

  /* Convert token to internal form (in yy_channelchar1) for indexing tables with */

  if (yy_channelchar <= 0)		/* This means end of input_channel. */
    {
      yy_channelchar1 = 0;
      yy_channelchar = YY_channelEOF;		/* Don't call YY_channelLEX any more */

#if YY_channelDEBUG != 0
      if (yy_channeldebug)
	fprintf(stderr, "Now at end of input_channel.\n");
#endif
    }
  else
    {
      yy_channelchar1 = YY_channelTRANSLATE(yy_channelchar);

#if YY_channelDEBUG != 0
      if (yy_channeldebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yy_channelchar, yy_channeltname[yy_channelchar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YY_channelPRINT
	  YY_channelPRINT (stderr, yy_channelchar, yy_channellval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yy_channeln += yy_channelchar1;
  if (yy_channeln < 0 || yy_channeln > YY_channelLAST || yy_channelcheck[yy_channeln] != yy_channelchar1)
    goto yy_channeldefault;

  yy_channeln = yy_channeltable[yy_channeln];

  /* yy_channeln is what to do for this token type in this state.
     Negative => reduce, -yy_channeln is rule number.
     Positive => shift, yy_channeln is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yy_channeln < 0)
    {
      if (yy_channeln == YY_channelFLAG)
	goto yy_channelerrlab;
      yy_channeln = -yy_channeln;
      goto yy_channelreduce;
    }
  else if (yy_channeln == 0)
    goto yy_channelerrlab;

  if (yy_channeln == YY_channelFINAL)
    YY_channelACCEPT;

  /* Shift the lookahead token.  */

#if YY_channelDEBUG != 0
  if (yy_channeldebug)
    fprintf(stderr, "Shifting token %d (%s), ", yy_channelchar, yy_channeltname[yy_channelchar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yy_channelchar != YY_channelEOF)
    yy_channelchar = YY_channelEMPTY;

  *++yy_channelvsp = yy_channellval;
#ifdef YY_channelLSP_NEEDED
  *++yy_channellsp = yy_channellloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yy_channelerrstatus) yy_channelerrstatus--;

  yy_channelstate = yy_channeln;
  goto yy_channelnewstate;

/* Do the default action for the current state.  */
yy_channeldefault:

  yy_channeln = yy_channeldefact[yy_channelstate];
  if (yy_channeln == 0)
    goto yy_channelerrlab;

/* Do a reduction.  yy_channeln is the number of a rule to reduce with.  */
yy_channelreduce:
  yy_channellen = yy_channelr2[yy_channeln];
  if (yy_channellen > 0)
    yy_channelval = yy_channelvsp[1-yy_channellen]; /* implement default value of the action */

#if YY_channelDEBUG != 0
  if (yy_channeldebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yy_channeln, yy_channelrline[yy_channeln]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yy_channelprhs[yy_channeln]; yy_channelrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yy_channeltname[yy_channelrhs[i]]);
      fprintf (stderr, " -> %s\n", yy_channeltname[yy_channelr1[yy_channeln]]);
    }
#endif


  switch (yy_channeln) {

case 3:
#line 65 "channel.y"
{int cnt;
		 /* 
	 	  * Defaults filling
	 	  * FIELDS must be specified 
	 	  */
		 channel[channel_handle].fields = yy_channelvsp[0].myint;
		 if (channel[channel_handle].fields > 0)
		   {
		     channel[channel_handle].cnt = 0;
		     channel[channel_handle].global_cnt = 0;
		     channel[channel_handle].w_resolution[0] = 256;
		     channel[channel_handle].w_resolution[1] = 256;
		     channel[channel_handle].start_time = 0.;
		     channel[channel_handle].s_time = 0;
		     channel[channel_handle].duration_time = 1000.;
		     channel[channel_handle].d_time = 0.;
		     channel[channel_handle].print_format = string ("line");
		     channel[channel_handle].type = string ("graphic");
		     channel[channel_handle].on_leave_w = string ("noevent");
		     channel[channel_handle].list = 
		       (struct LIST *)allocate (sizeof (struct LIST), PERM);
		     channel[channel_handle].list_tmp = 
		       (struct LIST *)allocate (sizeof (struct LIST), PERM);
		     channel[channel_handle].member =
		       (struct FIELD *)allocate (channel[channel_handle].fields 
						 * sizeof (struct FIELD), PERM);
		     for (cnt = 0; cnt < channel[channel_handle].fields; cnt++)
		       {
			 channel[channel_handle].member[cnt].style = 0xffffffL;
			 channel[channel_handle].member[cnt].upper = 1;
			 channel[channel_handle].member[cnt].lower = -1;
			}};
    break;}
case 4:
#line 99 "channel.y"
{
#if 0
		  free (channel[channel_handle].type);
#endif
		  channel[channel_handle].type = yy_channelvsp[0].mystring;;
    break;}
case 5:
#line 106 "channel.y"
{
#if 0
		  free (channel[channel_handle].print_format);
#endif
		  channel[channel_handle].print_format = yy_channelvsp[0].mystring;;
    break;}
case 6:
#line 113 "channel.y"
{
#if 0
		  free (channel[channel_handle].on_leave_w);
#endif
		  channel[channel_handle].on_leave_w = yy_channelvsp[0].mystring;;
    break;}
case 7:
#line 120 "channel.y"
{if (0 == (strcmp (yy_channelvsp[0].mystring, "input_channel")))
		   channel[channel_handle].direction = 0;
		 else
		   {
		     if (0 == (strcmp (yy_channelvsp[0].mystring, "output_channel")))
		       channel[channel_handle].direction = 1;
		     else
		       {
			 fprintfx (stderr, "bad direction\n");
			 exit (0);
			}
		};
    break;}
case 10:
#line 138 "channel.y"
{channel[channel_handle].w_resolution[0] = yy_channelvsp[-1].myint; channel[channel_handle].w_resolution[1] = yy_channelvsp[0].myint;;
    break;}
case 11:
#line 141 "channel.y"
{if (channel[channel_handle].fields > yy_channelvsp[-3].myint)
		   channel[channel_handle].member[yy_channelvsp[-3].myint].lower = yy_channelvsp[0].mydouble;
		 else
		   {
		     fprintfx (stderr, "invalid subscript\n");
		     exit (0);
		   };
    break;}
case 12:
#line 150 "channel.y"
{if (channel[channel_handle].fields > yy_channelvsp[-3].myint)
		   channel[channel_handle].member[yy_channelvsp[-3].myint].upper = yy_channelvsp[0].mydouble;
		 else
		   {
		     fprintfx (stderr, "invalid subscript\n");
		     exit (0);
		   };
    break;}
case 13:
#line 159 "channel.y"
{if (channel[channel_handle].fields > yy_channelvsp[-3].myint)
		   channel[channel_handle].member[yy_channelvsp[-3].myint].style=(unsigned long)yy_channelvsp[0].myint;
		 else
		   {
		     fprintfx (stderr,"invalid subscript\n");
		     exit (0);
		   };
    break;}
case 14:
#line 168 "channel.y"
{fprintfx (stderr,"error in opening channel\n"); return (-2);;
    break;}
case 15:
#line 173 "channel.y"
{channel[channel_handle].duration_time = yy_channelvsp[0].mydouble; channel[channel_handle].d_time = 0.;;
    break;}
case 16:
#line 176 "channel.y"
{channel[channel_handle].duration_time = 16.; channel[channel_handle].d_time = 1.;;
    break;}
case 17:
#line 181 "channel.y"
{channel[channel_handle].start_time = yy_channelvsp[0].mydouble; channel[channel_handle].s_time = 0;;
    break;}
case 18:
#line 184 "channel.y"
{channel[channel_handle].s_time = 1;;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 465 "/usr/local/gnu/lib/bison.simple"

  yy_channelvsp -= yy_channellen;
  yy_channelssp -= yy_channellen;
#ifdef YY_channelLSP_NEEDED
  yy_channellsp -= yy_channellen;
#endif

#if YY_channelDEBUG != 0
  if (yy_channeldebug)
    {
      short *ssp1 = yy_channelss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yy_channelssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yy_channelvsp = yy_channelval;

#ifdef YY_channelLSP_NEEDED
  yy_channellsp++;
  if (yy_channellen == 0)
    {
      yy_channellsp->first_line = yy_channellloc.first_line;
      yy_channellsp->first_column = yy_channellloc.first_column;
      yy_channellsp->last_line = (yy_channellsp-1)->last_line;
      yy_channellsp->last_column = (yy_channellsp-1)->last_column;
      yy_channellsp->text = 0;
    }
  else
    {
      yy_channellsp->last_line = (yy_channellsp+yy_channellen-1)->last_line;
      yy_channellsp->last_column = (yy_channellsp+yy_channellen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yy_channeln = yy_channelr1[yy_channeln];

  yy_channelstate = yy_channelpgoto[yy_channeln - YY_channelNTBASE] + *yy_channelssp;
  if (yy_channelstate >= 0 && yy_channelstate <= YY_channelLAST && yy_channelcheck[yy_channelstate] == *yy_channelssp)
    yy_channelstate = yy_channeltable[yy_channelstate];
  else
    yy_channelstate = yy_channeldefgoto[yy_channeln - YY_channelNTBASE];

  goto yy_channelnewstate;

yy_channelerrlab:   /* here on detecting error */

  if (! yy_channelerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yy_channelnerrs;

#ifdef YY_channelERROR_VERBOSE
      yy_channeln = yy_channelpact[yy_channelstate];

      if (yy_channeln > YY_channelFLAG && yy_channeln < YY_channelLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yy_channeln if nec to avoid negative indexes in yy_channelcheck.  */
	  for (x = (yy_channeln < 0 ? -yy_channeln : 0);
	       x < (sizeof(yy_channeltname) / sizeof(char *)); x++)
	    if (yy_channelcheck[x + yy_channeln] == x)
	      size += strlen(yy_channeltname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yy_channeln < 0 ? -yy_channeln : 0);
		       x < (sizeof(yy_channeltname) / sizeof(char *)); x++)
		    if (yy_channelcheck[x + yy_channeln] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yy_channeltname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yy_channelerror(msg);
	      free(msg);
	    }
	  else
	    yy_channelerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YY_channelERROR_VERBOSE */
	yy_channelerror("parse error");
    }

  goto yy_channelerrlab1;
yy_channelerrlab1:   /* here on error raised explicitly by an action */

  if (yy_channelerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input_channel */
      if (yy_channelchar == YY_channelEOF)
	YY_channelABORT;

#if YY_channelDEBUG != 0
      if (yy_channeldebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yy_channelchar, yy_channeltname[yy_channelchar1]);
#endif

      yy_channelchar = YY_channelEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yy_channelerrstatus = 3;		/* Each real token shifted decrements this */

  goto yy_channelerrhandle;

yy_channelerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yy_channeln = yy_channeldefact[yy_channelstate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yy_channeln) goto yy_channeldefault;
#endif

yy_channelerrpop:   /* pop the current state because it cannot handle the error token */

  if (yy_channelssp == yy_channelss) YY_channelABORT;
  yy_channelvsp--;
  yy_channelstate = *--yy_channelssp;
#ifdef YY_channelLSP_NEEDED
  yy_channellsp--;
#endif

#if YY_channelDEBUG != 0
  if (yy_channeldebug)
    {
      short *ssp1 = yy_channelss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yy_channelssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yy_channelerrhandle:

  yy_channeln = yy_channelpact[yy_channelstate];
  if (yy_channeln == YY_channelFLAG)
    goto yy_channelerrdefault;

  yy_channeln += YY_channelTERROR;
  if (yy_channeln < 0 || yy_channeln > YY_channelLAST || yy_channelcheck[yy_channeln] != YY_channelTERROR)
    goto yy_channelerrdefault;

  yy_channeln = yy_channeltable[yy_channeln];
  if (yy_channeln < 0)
    {
      if (yy_channeln == YY_channelFLAG)
	goto yy_channelerrpop;
      yy_channeln = -yy_channeln;
      goto yy_channelreduce;
    }
  else if (yy_channeln == 0)
    goto yy_channelerrpop;

  if (yy_channeln == YY_channelFINAL)
    YY_channelACCEPT;

#if YY_channelDEBUG != 0
  if (yy_channeldebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yy_channelvsp = yy_channellval;
#ifdef YY_channelLSP_NEEDED
  *++yy_channellsp = yy_channellloc;
#endif

  yy_channelstate = yy_channeln;
  goto yy_channelnewstate;
}
#line 188 "channel.y"

