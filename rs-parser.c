
/*  A Bison parser, made from run_str.y
 by  GNU Bison version 1.25
  */

#define YY_rs_BISON 1  /* Identify Bison output_rs_.  */

#define	NUMBER	258
#define	STRING	259
#define	BGC	260
#define	HELP	261
#define	COMPILE	262
#define	WARNING	263
#define	WARRANTY	264
#define	COPYING	265
#define	VERBOSE	266
#define	VERSION	267
#define	WARNING_INHIBIT	268
#define	WARNING_COMMENT	269
#define	WARNING_FORMAT	270
#define	WARNING_IMPLICIT	271
#define	WARNING_RETURN_TYPE	272
#define	WARNING_TRIGRAPHS	273
#define	WARNING_UNUSED	274
#define	WARNING_UNINITIAL_rs_IZED	275
#define	WARNING_EXTRA	276
#define	WARNING_AGGREGATE_RETURN	277
#define	WARNING_ERROR	278
#define	HANDLE_MAIN	279
#define	CALL_BY_REFERENCE	280
#define	CALL_BY_VALUE	281
#define	NO_CALL_BY_REFERENCE	282
#define	NO_CALL_BY_VALUE	283
#define	DUMP_YACC	284
#define	DBG_INFO	285


#line 27 "run_str.y"
typedef union {
	int myy_rs_int;
	char *myy_rs_string;
	} YY_rs_STYPE;
#line 51 "run_str.y"

#include <stdio.h>
#include "flags.h"
int argc_counter = 0;
extern int bc;
char *argvv[100];

#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif
extern int help PROTO((int));
int yy_rs_lex PROTO((void));
void error_message PROTO((int));
int yy_rs_error PROTO((char *));

int 
yy_rs_error (s)
  char *s;
{
  return (0);
}

#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YY_rs_FINAL		34
#define	YY_rs_FLAG		-32768
#define	YY_rs_NTBASE	32

#define YY_rs_TRANSLATE(x) ((unsigned)(x) <= 285 ? yy_rs_translate[x] : 34)

static const char yy_rs_translate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
    13,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    14,    15,    16,
    17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
    27,    28,    29,    30,    31
};

#if YY_rs_DEBUG != 0
static const short yy_rs_prhs[] = {     0,
     0,     3,     4,     8,    10,    12,    14,    16,    18,    20,
    22,    24,    26,    28,    30,    32,    34,    36,    38,    40,
    42,    44,    46,    48,    50,    52,    54,    56,    58,    60
};

static const short yy_rs_rhs[] = {    32,
    33,     0,     0,     5,    13,     3,     0,    26,     0,    28,
     0,    27,     0,    29,     0,     7,     0,    10,     0,    30,
     0,    31,     0,    25,     0,     6,     0,     4,     0,    11,
     0,    12,     0,     8,     0,    23,     0,    15,     0,    24,
     0,    22,     0,    16,     0,    17,     0,    14,     0,    18,
     0,    19,     0,    21,     0,    20,     0,     9,     0,     1,
     0
};

#endif

#if YY_rs_DEBUG != 0
static const short yy_rs_rline[] = { 0,
    80,    82,    85,    88,    90,    92,    94,    96,    99,   104,
   110,   113,   116,   122,   125,   131,   137,   140,   143,   146,
   149,   152,   155,   158,   161,   164,   167,   170,   173,   179
};
#endif


#if YY_rs_DEBUG != 0 || defined (YY_rs_ERROR_VERBOSE)

static const char * const yy_rs_tname[] = {   "$","error","$undefined.","NUMBER",
"STRING","BGC","HELP","COMPILE","WARNING","WARRANTY","COPYING","VERBOSE","VERSION",
"'='","WARNING_INHIBIT","WARNING_COMMENT","WARNING_FORMAT","WARNING_IMPLICIT",
"WARNING_RETURN_TYPE","WARNING_TRIGRAPHS","WARNING_UNUSED","WARNING_UNINITIAL_rs_IZED",
"WARNING_EXTRA","WARNING_AGGREGATE_RETURN","WARNING_ERROR","HANDLE_MAIN","CALL_BY_REFERENCE",
"CALL_BY_VALUE","NO_CALL_BY_REFERENCE","NO_CALL_BY_VALUE","DUMP_YACC","DBG_INFO",
"list_stat_0","stat_0", NULL
};
#endif

static const short yy_rs_r1[] = {     0,
    32,    32,    33,    33,    33,    33,    33,    33,    33,    33,
    33,    33,    33,    33,    33,    33,    33,    33,    33,    33,
    33,    33,    33,    33,    33,    33,    33,    33,    33,    33
};

static const short yy_rs_r2[] = {     0,
     2,     0,     3,     1,     1,     1,     1,     1,     1,     1,
     1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
     1,     1,     1,     1,     1,     1,     1,     1,     1,     1
};

static const short yy_rs_defact[] = {     2,
     0,    30,    14,     0,    13,     8,    17,    29,     9,    15,
    16,    24,    19,    22,    23,    25,    26,    28,    27,    21,
    18,    20,    12,     4,     6,     5,     7,    10,    11,     1,
     0,     3,     0,     0
};

static const short yy_rs_defgoto[] = {     1,
    30
};

static const short yy_rs_pact[] = {-32768,
     0,-32768,-32768,   -11,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,
    10,-32768,     3,-32768
};

static const short yy_rs_pgoto[] = {-32768,
-32768
};


#define	YY_rs_LAST		31


static const short yy_rs_table[] = {    33,
     2,    31,    34,     3,     4,     5,     6,     7,     8,     9,
    10,    11,    32,    12,    13,    14,    15,    16,    17,    18,
    19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
    29
};

static const short yy_rs_check[] = {     0,
     1,    13,     0,     4,     5,     6,     7,     8,     9,    10,
    11,    12,     3,    14,    15,    16,    17,    18,    19,    20,
    21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
    31
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/usr/share/misc/bison.simple"

/* Skeleton output_rs_ parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output_rs_ file, you may use that output_rs_ file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi)
#include <alloca.h>
#else /* not sparc */
#if defined (MSDOS) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
 #pragma alloca
#else /* not MSDOS, __TURBOC__, or _AIX */
#ifdef __hpux
#ifdef __cplusplus
extern "C" {
void *alloca (unsigned int);
};
#else /* not __cplusplus */
void *alloca ();
#endif /* not __cplusplus */
#endif /* __hpux */
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif /* alloca not defined.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yy_rs_errok		(yy_rs_errstatus = 0)
#define yy_rs_clearin	(yy_rs_char = YY_rs_EMPTY)
#define YY_rs_EMPTY		-2
#define YY_rs_EOF		0
#define YY_rs_ACCEPT	return(0)
#define YY_rs_ABORT 	return(1)
#define YY_rs_ERROR		goto yy_rs_errlab1
/* Like YY_rs_ERROR except do call yy_rs_error.
   This remains here temporarily to ease the
   transition to the new meaning of YY_rs_ERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YY_rs_FAIL		goto yy_rs_errlab
#define YY_rs_RECOVERING()  (!!yy_rs_errstatus)
#define YY_rs_BACKUP(token, value) \
do								\
  if (yy_rs_char == YY_rs_EMPTY && yy_rs_len == 1)				\
    { yy_rs_char = (token), yy_rs_lval = (value);			\
      yy_rs_char1 = YY_rs_TRANSLATE (yy_rs_char);				\
      YY_rs_POPSTACK;						\
      goto yy_rs_backup;						\
    }								\
  else								\
    { yy_rs_error ("syntax error: cannot back up"); YY_rs_ERROR; }	\
while (0)

#define YY_rs_TERROR	1
#define YY_rs_ERRCODE	256

#ifndef YY_rs_PURE
#define YY_rs_LEX		yy_rs_lex()
#endif

#ifdef YY_rs_PURE
#ifdef YY_rs_LSP_NEEDED
#ifdef YY_rs_LEX_PARAM
#define YY_rs_LEX		yy_rs_lex(&yy_rs_lval, &yy_rs_lloc, YY_rs_LEX_PARAM)
#else
#define YY_rs_LEX		yy_rs_lex(&yy_rs_lval, &yy_rs_lloc)
#endif
#else /* not YY_rs_LSP_NEEDED */
#ifdef YY_rs_LEX_PARAM
#define YY_rs_LEX		yy_rs_lex(&yy_rs_lval, YY_rs_LEX_PARAM)
#else
#define YY_rs_LEX		yy_rs_lex(&yy_rs_lval)
#endif
#endif /* not YY_rs_LSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YY_rs_PURE

int	yy_rs_char;			/*  the lookahead symbol		*/
YY_rs_STYPE	yy_rs_lval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YY_rs_LSP_NEEDED
YY_rs_LTYPE yy_rs_lloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yy_rs_nerrs;			/*  number of parse errors so far       */
#endif  /* not YY_rs_PURE */

#if YY_rs_DEBUG != 0
int yy_rs_debug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YY_rs_INITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YY_rs_INITDEPTH
#define YY_rs_INITDEPTH 200
#endif

/*  YY_rs_MAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YY_rs_MAXDEPTH == 0
#undef YY_rs_MAXDEPTH
#endif

#ifndef YY_rs_MAXDEPTH
#define YY_rs_MAXDEPTH 10000
#endif

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
int yy_rs_parse (void);
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_rs__memcpy(TO,FROM,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_rs__memcpy (to, from, count)
     char *to;
     char *from;
     int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_rs__memcpy (char *to, char *from, int count)
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 196 "/usr/share/misc/bison.simple"

/* The user can define YY_rs_PARSE_PARAM as the name of an argument to be passed
   into yy_rs_parse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YY_rs_PARSE_PARAM
#ifdef __cplusplus
#define YY_rs_PARSE_PARAM_ARG void *YY_rs_PARSE_PARAM
#define YY_rs_PARSE_PARAM_DECL
#else /* not __cplusplus */
#define YY_rs_PARSE_PARAM_ARG YY_rs_PARSE_PARAM
#define YY_rs_PARSE_PARAM_DECL void *YY_rs_PARSE_PARAM;
#endif /* not __cplusplus */
#else /* not YY_rs_PARSE_PARAM */
#define YY_rs_PARSE_PARAM_ARG
#define YY_rs_PARSE_PARAM_DECL
#endif /* not YY_rs_PARSE_PARAM */

int
yy_rs_parse(YY_rs_PARSE_PARAM_ARG)
     YY_rs_PARSE_PARAM_DECL
{
  register int yy_rs_state;
  register int yy_rs_n;
  register short *yy_rs_ssp;
  register YY_rs_STYPE *yy_rs_vsp;
  int yy_rs_errstatus;	/*  number of tokens to shift before error messages enabled */
  int yy_rs_char1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yy_rs_ssa[YY_rs_INITDEPTH];	/*  the state stack			*/
  YY_rs_STYPE yy_rs_vsa[YY_rs_INITDEPTH];	/*  the semantic value stack		*/

  short *yy_rs_ss = yy_rs_ssa;		/*  refer to the stacks thru separate pointers */
  YY_rs_STYPE *yy_rs_vs = yy_rs_vsa;	/*  to allow yy_rs_overflow to reallocate them elsewhere */

#ifdef YY_rs_LSP_NEEDED
  YY_rs_LTYPE yy_rs_lsa[YY_rs_INITDEPTH];	/*  the location stack			*/
  YY_rs_LTYPE *yy_rs_ls = yy_rs_lsa;
  YY_rs_LTYPE *yy_rs_lsp;

#define YY_rs_POPSTACK   (yy_rs_vsp--, yy_rs_ssp--, yy_rs_lsp--)
#else
#define YY_rs_POPSTACK   (yy_rs_vsp--, yy_rs_ssp--)
#endif

  int yy_rs_stacksize = YY_rs_INITDEPTH;

#ifdef YY_rs_PURE
  int yy_rs_char;
  YY_rs_STYPE yy_rs_lval;
  int yy_rs_nerrs;
#ifdef YY_rs_LSP_NEEDED
  YY_rs_LTYPE yy_rs_lloc;
#endif
#endif

  YY_rs_STYPE yy_rs_val;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yy_rs_len;

#if YY_rs_DEBUG != 0
  if (yy_rs_debug)
    fprintf(stderr, "Starting parse\n");
#endif

  yy_rs_state = 0;
  yy_rs_errstatus = 0;
  yy_rs_nerrs = 0;
  yy_rs_char = YY_rs_EMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yy_rs_ssp = yy_rs_ss - 1;
  yy_rs_vsp = yy_rs_vs;
#ifdef YY_rs_LSP_NEEDED
  yy_rs_lsp = yy_rs_ls;
#endif

/* Push a new state, which is found in  yy_rs_state  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yy_rs_newstate:

  *++yy_rs_ssp = yy_rs_state;

  if (yy_rs_ssp >= yy_rs_ss + yy_rs_stacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YY_rs_STYPE *yy_rs_vs1 = yy_rs_vs;
      short *yy_rs_ss1 = yy_rs_ss;
#ifdef YY_rs_LSP_NEEDED
      YY_rs_LTYPE *yy_rs_ls1 = yy_rs_ls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yy_rs_ssp - yy_rs_ss + 1;

#ifdef yy_rs_overflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YY_rs_LSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yy_rs_overflow is a macro.  */
      yy_rs_overflow("parser stack overflow",
		 &yy_rs_ss1, size * sizeof (*yy_rs_ssp),
		 &yy_rs_vs1, size * sizeof (*yy_rs_vsp),
		 &yy_rs_ls1, size * sizeof (*yy_rs_lsp),
		 &yy_rs_stacksize);
#else
      yy_rs_overflow("parser stack overflow",
		 &yy_rs_ss1, size * sizeof (*yy_rs_ssp),
		 &yy_rs_vs1, size * sizeof (*yy_rs_vsp),
		 &yy_rs_stacksize);
#endif

      yy_rs_ss = yy_rs_ss1; yy_rs_vs = yy_rs_vs1;
#ifdef YY_rs_LSP_NEEDED
      yy_rs_ls = yy_rs_ls1;
#endif
#else /* no yy_rs_overflow */
      /* Extend the stack our own way.  */
      if (yy_rs_stacksize >= YY_rs_MAXDEPTH)
	{
	  yy_rs_error("parser stack overflow");
	  return 2;
	}
      yy_rs_stacksize *= 2;
      if (yy_rs_stacksize > YY_rs_MAXDEPTH)
	yy_rs_stacksize = YY_rs_MAXDEPTH;
      yy_rs_ss = (short *) alloca (yy_rs_stacksize * sizeof (*yy_rs_ssp));
      __yy_rs__memcpy ((char *)yy_rs_ss, (char *)yy_rs_ss1, size * sizeof (*yy_rs_ssp));
      yy_rs_vs = (YY_rs_STYPE *) alloca (yy_rs_stacksize * sizeof (*yy_rs_vsp));
      __yy_rs__memcpy ((char *)yy_rs_vs, (char *)yy_rs_vs1, size * sizeof (*yy_rs_vsp));
#ifdef YY_rs_LSP_NEEDED
      yy_rs_ls = (YY_rs_LTYPE *) alloca (yy_rs_stacksize * sizeof (*yy_rs_lsp));
      __yy_rs__memcpy ((char *)yy_rs_ls, (char *)yy_rs_ls1, size * sizeof (*yy_rs_lsp));
#endif
#endif /* no yy_rs_overflow */

      yy_rs_ssp = yy_rs_ss + size - 1;
      yy_rs_vsp = yy_rs_vs + size - 1;
#ifdef YY_rs_LSP_NEEDED
      yy_rs_lsp = yy_rs_ls + size - 1;
#endif

#if YY_rs_DEBUG != 0
      if (yy_rs_debug)
	fprintf(stderr, "Stack size increased to %d\n", yy_rs_stacksize);
#endif

      if (yy_rs_ssp >= yy_rs_ss + yy_rs_stacksize - 1)
	YY_rs_ABORT;
    }

#if YY_rs_DEBUG != 0
  if (yy_rs_debug)
    fprintf(stderr, "Entering state %d\n", yy_rs_state);
#endif

  goto yy_rs_backup;
 yy_rs_backup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yy_rs_resume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yy_rs_n = yy_rs_pact[yy_rs_state];
  if (yy_rs_n == YY_rs_FLAG)
    goto yy_rs_default;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yy_rs_char is either YY_rs_EMPTY or YY_rs_EOF
     or a valid token in external form.  */

  if (yy_rs_char == YY_rs_EMPTY)
    {
#if YY_rs_DEBUG != 0
      if (yy_rs_debug)
	fprintf(stderr, "Reading a token: ");
#endif
      yy_rs_char = YY_rs_LEX;
    }

  /* Convert token to internal form (in yy_rs_char1) for indexing tables with */

  if (yy_rs_char <= 0)		/* This means end of input_rs_. */
    {
      yy_rs_char1 = 0;
      yy_rs_char = YY_rs_EOF;		/* Don't call YY_rs_LEX any more */

#if YY_rs_DEBUG != 0
      if (yy_rs_debug)
	fprintf(stderr, "Now at end of input_rs_.\n");
#endif
    }
  else
    {
      yy_rs_char1 = YY_rs_TRANSLATE(yy_rs_char);

#if YY_rs_DEBUG != 0
      if (yy_rs_debug)
	{
	  fprintf (stderr, "Next token is %d (%s", yy_rs_char, yy_rs_tname[yy_rs_char1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YY_rs_PRINT
	  YY_rs_PRINT (stderr, yy_rs_char, yy_rs_lval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yy_rs_n += yy_rs_char1;
  if (yy_rs_n < 0 || yy_rs_n > YY_rs_LAST || yy_rs_check[yy_rs_n] != yy_rs_char1)
    goto yy_rs_default;

  yy_rs_n = yy_rs_table[yy_rs_n];

  /* yy_rs_n is what to do for this token type in this state.
     Negative => reduce, -yy_rs_n is rule number.
     Positive => shift, yy_rs_n is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yy_rs_n < 0)
    {
      if (yy_rs_n == YY_rs_FLAG)
	goto yy_rs_errlab;
      yy_rs_n = -yy_rs_n;
      goto yy_rs_reduce;
    }
  else if (yy_rs_n == 0)
    goto yy_rs_errlab;

  if (yy_rs_n == YY_rs_FINAL)
    YY_rs_ACCEPT;

  /* Shift the lookahead token.  */

#if YY_rs_DEBUG != 0
  if (yy_rs_debug)
    fprintf(stderr, "Shifting token %d (%s), ", yy_rs_char, yy_rs_tname[yy_rs_char1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yy_rs_char != YY_rs_EOF)
    yy_rs_char = YY_rs_EMPTY;

  *++yy_rs_vsp = yy_rs_lval;
#ifdef YY_rs_LSP_NEEDED
  *++yy_rs_lsp = yy_rs_lloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yy_rs_errstatus) yy_rs_errstatus--;

  yy_rs_state = yy_rs_n;
  goto yy_rs_newstate;

/* Do the default action for the current state.  */
yy_rs_default:

  yy_rs_n = yy_rs_defact[yy_rs_state];
  if (yy_rs_n == 0)
    goto yy_rs_errlab;

/* Do a reduction.  yy_rs_n is the number of a rule to reduce with.  */
yy_rs_reduce:
  yy_rs_len = yy_rs_r2[yy_rs_n];
  if (yy_rs_len > 0)
    yy_rs_val = yy_rs_vsp[1-yy_rs_len]; /* implement default value of the action */

#if YY_rs_DEBUG != 0
  if (yy_rs_debug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yy_rs_n, yy_rs_rline[yy_rs_n]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yy_rs_prhs[yy_rs_n]; yy_rs_rhs[i] > 0; i++)
	fprintf (stderr, "%s ", yy_rs_tname[yy_rs_rhs[i]]);
      fprintf (stderr, " -> %s\n", yy_rs_tname[yy_rs_r1[yy_rs_n]]);
    }
#endif


  switch (yy_rs_n) {

case 3:
#line 86 "run_str.y"
{bc = yy_rs_vsp[0].myy_rs_int;;
    break;}
case 4:
#line 89 "run_str.y"
{call_by_reference = 1;;
    break;}
case 5:
#line 91 "run_str.y"
{call_by_reference = 0;;
    break;}
case 6:
#line 93 "run_str.y"
{call_by_value = 1;;
    break;}
case 7:
#line 95 "run_str.y"
{call_by_value = 0;;
    break;}
case 8:
#line 97 "run_str.y"
{no_compile_only = 0;;
    break;}
case 9:
#line 100 "run_str.y"
{
		  if (help (2) == -1)
		    return (-1);
		;
    break;}
case 10:
#line 105 "run_str.y"
{
		  extern void dump_yacc PROTO((void));
		  dump_yacc ();
		;
    break;}
case 11:
#line 111 "run_str.y"
{dbg_symbols = 1;;
    break;}
case 12:
#line 114 "run_str.y"
{handle_main = 1;;
    break;}
case 13:
#line 117 "run_str.y"
{
		  if (help (1) == -1)
		    return (-1);
	       	;
    break;}
case 14:
#line 123 "run_str.y"
{argvv[++argc_counter] = yy_rs_vsp[0].myy_rs_string;;
    break;}
case 15:
#line 126 "run_str.y"
{
		  if (help (4) == -1)
		    return (-1);
		;
    break;}
case 16:
#line 132 "run_str.y"
{
		  if (help (5) == -1)
		    return (-1);
		;
    break;}
case 17:
#line 138 "run_str.y"
{warning_yes = 1;;
    break;}
case 18:
#line 141 "run_str.y"
{warning_aggregate_return = 1;;
    break;}
case 19:
#line 144 "run_str.y"
{warning_comment = 1;;
    break;}
case 20:
#line 147 "run_str.y"
{warnings_are_errors = 1;;
    break;}
case 21:
#line 150 "run_str.y"
{warning_extra = 1;;
    break;}
case 22:
#line 153 "run_str.y"
{warning_format = 1;;
    break;}
case 23:
#line 156 "run_str.y"
{warning_implicit = 1;;
    break;}
case 24:
#line 159 "run_str.y"
{warning_inhibit = 1;;
    break;}
case 25:
#line 162 "run_str.y"
{warning_return_type = 1;;
    break;}
case 26:
#line 165 "run_str.y"
{warning_trigraphs = 1;;
    break;}
case 27:
#line 168 "run_str.y"
{warning_uninitialized = 1;;
    break;}
case 28:
#line 171 "run_str.y"
{warning_unused = 1;;
    break;}
case 29:
#line 174 "run_str.y"
{
		  if (help (3) == -1)
		    return (-1);
		;
    break;}
case 30:
#line 180 "run_str.y"
{
		  error_message (7000); return (-1);
		;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 498 "/usr/share/misc/bison.simple"

  yy_rs_vsp -= yy_rs_len;
  yy_rs_ssp -= yy_rs_len;
#ifdef YY_rs_LSP_NEEDED
  yy_rs_lsp -= yy_rs_len;
#endif

#if YY_rs_DEBUG != 0
  if (yy_rs_debug)
    {
      short *ssp1 = yy_rs_ss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yy_rs_ssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yy_rs_vsp = yy_rs_val;

#ifdef YY_rs_LSP_NEEDED
  yy_rs_lsp++;
  if (yy_rs_len == 0)
    {
      yy_rs_lsp->first_line = yy_rs_lloc.first_line;
      yy_rs_lsp->first_column = yy_rs_lloc.first_column;
      yy_rs_lsp->last_line = (yy_rs_lsp-1)->last_line;
      yy_rs_lsp->last_column = (yy_rs_lsp-1)->last_column;
      yy_rs_lsp->text = 0;
    }
  else
    {
      yy_rs_lsp->last_line = (yy_rs_lsp+yy_rs_len-1)->last_line;
      yy_rs_lsp->last_column = (yy_rs_lsp+yy_rs_len-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yy_rs_n = yy_rs_r1[yy_rs_n];

  yy_rs_state = yy_rs_pgoto[yy_rs_n - YY_rs_NTBASE] + *yy_rs_ssp;
  if (yy_rs_state >= 0 && yy_rs_state <= YY_rs_LAST && yy_rs_check[yy_rs_state] == *yy_rs_ssp)
    yy_rs_state = yy_rs_table[yy_rs_state];
  else
    yy_rs_state = yy_rs_defgoto[yy_rs_n - YY_rs_NTBASE];

  goto yy_rs_newstate;

yy_rs_errlab:   /* here on detecting error */

  if (! yy_rs_errstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yy_rs_nerrs;

#ifdef YY_rs_ERROR_VERBOSE
      yy_rs_n = yy_rs_pact[yy_rs_state];

      if (yy_rs_n > YY_rs_FLAG && yy_rs_n < YY_rs_LAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yy_rs_n if nec to avoid negative indexes in yy_rs_check.  */
	  for (x = (yy_rs_n < 0 ? -yy_rs_n : 0);
	       x < (sizeof(yy_rs_tname) / sizeof(char *)); x++)
	    if (yy_rs_check[x + yy_rs_n] == x)
	      size += strlen(yy_rs_tname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yy_rs_n < 0 ? -yy_rs_n : 0);
		       x < (sizeof(yy_rs_tname) / sizeof(char *)); x++)
		    if (yy_rs_check[x + yy_rs_n] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yy_rs_tname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yy_rs_error(msg);
	      free(msg);
	    }
	  else
	    yy_rs_error ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YY_rs_ERROR_VERBOSE */
	yy_rs_error("parse error");
    }

  goto yy_rs_errlab1;
yy_rs_errlab1:   /* here on error raised explicitly by an action */

  if (yy_rs_errstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input_rs_ */
      if (yy_rs_char == YY_rs_EOF)
	YY_rs_ABORT;

#if YY_rs_DEBUG != 0
      if (yy_rs_debug)
	fprintf(stderr, "Discarding token %d (%s).\n", yy_rs_char, yy_rs_tname[yy_rs_char1]);
#endif

      yy_rs_char = YY_rs_EMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yy_rs_errstatus = 3;		/* Each real token shifted decrements this */

  goto yy_rs_errhandle;

yy_rs_errdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yy_rs_n = yy_rs_defact[yy_rs_state];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yy_rs_n) goto yy_rs_default;
#endif

yy_rs_errpop:   /* pop the current state because it cannot handle the error token */

  if (yy_rs_ssp == yy_rs_ss) YY_rs_ABORT;
  yy_rs_vsp--;
  yy_rs_state = *--yy_rs_ssp;
#ifdef YY_rs_LSP_NEEDED
  yy_rs_lsp--;
#endif

#if YY_rs_DEBUG != 0
  if (yy_rs_debug)
    {
      short *ssp1 = yy_rs_ss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yy_rs_ssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yy_rs_errhandle:

  yy_rs_n = yy_rs_pact[yy_rs_state];
  if (yy_rs_n == YY_rs_FLAG)
    goto yy_rs_errdefault;

  yy_rs_n += YY_rs_TERROR;
  if (yy_rs_n < 0 || yy_rs_n > YY_rs_LAST || yy_rs_check[yy_rs_n] != YY_rs_TERROR)
    goto yy_rs_errdefault;

  yy_rs_n = yy_rs_table[yy_rs_n];
  if (yy_rs_n < 0)
    {
      if (yy_rs_n == YY_rs_FLAG)
	goto yy_rs_errpop;
      yy_rs_n = -yy_rs_n;
      goto yy_rs_reduce;
    }
  else if (yy_rs_n == 0)
    goto yy_rs_errpop;

  if (yy_rs_n == YY_rs_FINAL)
    YY_rs_ACCEPT;

#if YY_rs_DEBUG != 0
  if (yy_rs_debug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yy_rs_vsp = yy_rs_lval;
#ifdef YY_rs_LSP_NEEDED
  *++yy_rs_lsp = yy_rs_lloc;
#endif

  yy_rs_state = yy_rs_n;
  goto yy_rs_newstate;
}
#line 185 "run_str.y"

