/*
    Clif - A C-like Interpreter Framework
    Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997 T. Hruz, L. Koren
    1998 L. Koren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
/*
 * geninstr.h
 *
 * Header of macros.
 * Defines instruction set of the
 * virtual machine.
 */

#ifndef _GENINSTR_H
#define _GENINSTR_H
          /* 
	   * Arithmetic and logical instructions.
	   */


#define GEN_ADDi ((struct OPERAND_0_mi *)kodp)->major = ADD;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp +=  sizeof (struct OPERAND_0_mi);

#define GEN_ADDss(const) ((struct OPERAND_1_i *)kodp)->major = ADD;\
((struct OPERAND_1_i *)kodp)->minor = 1;\
((struct OPERAND_1_i *)kodp)->num = const;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_ADDui ((struct OPERAND_0_mi *)kodp)->major = ADD;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp +=  sizeof (struct OPERAND_0_mi);

#define GEN_ADDli ((struct OPERAND_0_mi *)kodp)->major = ADD;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp +=  sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_ADDsi ((struct OPERAND_0_mi *)kodp)->major = ADD;\
((struct OPERAND_0_mi *)kodp)->minor = 4;\
kodp +=  sizeof (struct OPERAND_0_mi);
#endif

#define GEN_ADDlui ((struct OPERAND_0_mi *)kodp)->major = ADD;\
((struct OPERAND_0_mi *)kodp)->minor = 5;\
kodp +=  sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_ADDsui ((struct OPERAND_0_mi *)kodp)->major = ADD;\
((struct OPERAND_0_mi *)kodp)->minor = 6;\
kodp +=  sizeof (struct OPERAND_0_mi);
#endif

#define GEN_ADDd ((struct OPERAND_0_mi *)kodp)->major = ADD;\
((struct OPERAND_0_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ADDld ((struct OPERAND_0_mi *)kodp)->major = ADD;\
((struct OPERAND_0_mi *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ADDf ((struct OPERAND_0_mi *)kodp)->major = ADD;\
((struct OPERAND_0_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_ADDc ((struct OPERAND_0_mi *)kodp)->major = ADD;\
((struct OPERAND_0_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ADDsc ((struct OPERAND_0_mi *)kodp)->major = ADD;\
((struct OPERAND_0_mi *)kodp)->minor = 769;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ADDuc ((struct OPERAND_0_mi *)kodp)->major = ADD;\
((struct OPERAND_0_mi *)kodp)->minor = 770;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_ADD							\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_ADDi;							\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_ADDui;						\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_ADDli;						\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_ADDlui;						\
      break;							\
    case DOUB:							\
      GEN_ADDd;							\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_ADDld;						\
      break;							\
    case FLT:							\
      GEN_ADDf;							\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }



#define GEN_SUBi ((struct OPERAND_0_mi *)kodp)->major = SUB;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_SUBss(const) ((struct OPERAND_1_i *)kodp)->major = SUB;\
((struct OPERAND_1_i *)kodp)->num = const;\
((struct OPERAND_1_i *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_SUBui ((struct OPERAND_0_mi *)kodp)->major = SUB;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_SUBli ((struct OPERAND_0_mi *)kodp)->major = SUB;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_SUBlui ((struct OPERAND_0_mi *)kodp)->major = SUB;\
((struct OPERAND_0_mi *)kodp)->minor = 4;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_SUBd ((struct OPERAND_0_mi *)kodp)->major = SUB;\
((struct OPERAND_0_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_SUBld ((struct OPERAND_0_mi *)kodp)->major = SUB;\
((struct OPERAND_0_mi *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_SUBf ((struct OPERAND_0_mi *)kodp)->major = SUB;\
((struct OPERAND_0_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_SUBc ((struct OPERAND_0_mi *)kodp)->major = SUB;\
((struct OPERAND_0_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_SUB							\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_SUBi;							\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_SUBui;						\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_SUBli;						\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_SUBlui;						\
      break;							\
    case DOUB:							\
      GEN_SUBd;							\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_SUBld;						\
      break;							\
    case FLT:							\
      GEN_SUBf;							\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }



#define GEN_MULTi ((struct OPERAND_0_mi *)kodp)->major = MULT;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MULTui ((struct OPERAND_0_mi *)kodp)->major = MULT;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MULTli ((struct OPERAND_0_mi *)kodp)->major = MULT;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MULTlui ((struct OPERAND_0_mi *)kodp)->major = MULT;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MULTd ((struct OPERAND_0_mi *)kodp)->major = MULT;\
((struct OPERAND_0_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MULTld ((struct OPERAND_0_mi *)kodp)->major = MULT;\
((struct OPERAND_0_mi *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MULTf ((struct OPERAND_0_mi *)kodp)->major = MULT;\
((struct OPERAND_0_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_MULTc ((struct OPERAND_0_mi *)kodp)->major = MULT;\
((struct OPERAND_0_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_MULT						\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_MULTi;						\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_MULTui;						\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_MULTli;						\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_MULTlui;						\
      break;							\
    case DOUB:							\
      GEN_MULTd;						\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_MULTld;						\
      break;							\
    case FLT:							\
      GEN_MULTf;						\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }



#define GEN_DIVi ((struct OPERAND_0_mi *)kodp)->major = DIV;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_DIVui ((struct OPERAND_0_mi *)kodp)->major = DIV;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_DIVli ((struct OPERAND_0_mi *)kodp)->major = DIV;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_DIVlui ((struct OPERAND_0_mi *)kodp)->major = DIV;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_DIVd ((struct OPERAND_0_mi *)kodp)->major = DIV;\
((struct OPERAND_0_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_DIVld ((struct OPERAND_0_mi *)kodp)->major = DIV;\
((struct OPERAND_0_mi *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_DIVf ((struct OPERAND_0_mi *)kodp)->major = DIV;\
((struct OPERAND_0_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_DIVc ((struct OPERAND_0_mi *)kodp)->major = DIV;\
((struct OPERAND_0_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_DIV							\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_DIVi;							\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_DIVui;						\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_DIVli;						\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_DIVlui;						\
      break;							\
    case DOUB:							\
      GEN_DIVd;							\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_DIVld;						\
      break;							\
    case FLT:							\
      GEN_DIVf;							\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }



#define GEN_MOD ((struct OPERAND_0_ma *)kodp)->major = MOD;\
kodp +=  sizeof (struct OPERAND_0_ma);

#define GEN_ORi ((struct OPERAND_0_mi *)kodp)->major = OR;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ORui ((struct OPERAND_0_mi *)kodp)->major = OR;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ORli ((struct OPERAND_0_mi *)kodp)->major = OR;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ORlui ((struct OPERAND_0_mi *)kodp)->major = OR;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ORd ((struct OPERAND_0_mi *)kodp)->major = OR;\
((struct OPERAND_0_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ORld ((struct OPERAND_0_mi *)kodp)->major = OR;\
((struct OPERAND_0_mi *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ORf ((struct OPERAND_0_mi *)kodp)->major = OR;\
((struct OPERAND_0_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_ORc ((struct OPERAND_0_mi *)kodp)->major = OR;\
((struct OPERAND_0_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_OR							\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_ORi;							\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_ORui;							\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_ORli;							\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_ORlui;						\
      break;							\
    case DOUB:							\
      GEN_ORd;							\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_ORld;							\
      break;							\
    case FLT:							\
      GEN_ORf;							\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }

#define GEN_ANDi ((struct OPERAND_0_mi *)kodp)->major = AND;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ANDui ((struct OPERAND_0_mi *)kodp)->major = AND;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ANDli ((struct OPERAND_0_mi *)kodp)->major = AND;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ANDlui ((struct OPERAND_0_mi *)kodp)->major = AND;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ANDd ((struct OPERAND_0_mi *)kodp)->major = AND;\
((struct OPERAND_0_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ANDld ((struct OPERAND_0_mi *)kodp)->major = AND;\
((struct OPERAND_0_mi *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_ANDf ((struct OPERAND_0_mi *)kodp)->major = AND;\
((struct OPERAND_0_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_ANDc ((struct OPERAND_0_mi *)kodp)->major = AND;\
((struct OPERAND_0_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_AND							\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_ANDi;							\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_ANDui;						\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_ANDli;						\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_ANDlui;						\
      break;							\
    case DOUB:							\
      GEN_ANDd;							\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_ANDld;						\
      break;							\
    case FLT:							\
      GEN_ANDf;							\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }


#define GEN_ORB ((struct OPERAND_0_ma *)kodp)->major = ORB;\
kodp += sizeof (struct OPERAND_0_ma);

#define GEN_ANDB ((struct OPERAND_0_ma *)kodp)->major = ANDB;\
kodp += sizeof (struct OPERAND_0_ma);

#define GEN_EQi ((struct OPERAND_0_mi *)kodp)->major = EQ;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_EQui ((struct OPERAND_0_mi *)kodp)->major = EQ;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_EQli ((struct OPERAND_0_mi *)kodp)->major = EQ;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_EQlui ((struct OPERAND_0_mi *)kodp)->major = EQ;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_EQd ((struct OPERAND_0_mi *)kodp)->major = EQ;\
((struct OPERAND_0_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_EQld ((struct OPERAND_0_mi *)kodp)->major = EQ;\
((struct OPERAND_0_mi *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_EQf ((struct OPERAND_0_mi *)kodp)->major = EQ;\
((struct OPERAND_0_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_EQc ((struct OPERAND_0_mi *)kodp)->major = EQ;\
((struct OPERAND_0_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_EQ							\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_EQi;							\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_EQui;							\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_EQli;							\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_EQlui;						\
      break;							\
    case DOUB:							\
      GEN_EQd;							\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_EQld;							\
      break;							\
    case FLT:							\
      GEN_EQf;							\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }

#define GEN_LOi ((struct OPERAND_0_mi *)kodp)->major = LO;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_LOui ((struct OPERAND_0_mi *)kodp)->major = LO;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_LOli ((struct OPERAND_0_mi *)kodp)->major = LO;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_LOlui ((struct OPERAND_0_mi *)kodp)->major = LO;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_LOd ((struct OPERAND_0_mi *)kodp)->major = LO;\
((struct OPERAND_0_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_LOld ((struct OPERAND_0_mi *)kodp)->major = LO;\
((struct OPERAND_0_mi *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_LOf ((struct OPERAND_0_mi *)kodp)->major = LO;\
((struct OPERAND_0_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_LOc ((struct OPERAND_0_mi *)kodp)->major = LO;\
((struct OPERAND_0_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_LO							\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_LOi;							\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_LOui;							\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_LOli;							\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_LOlui;						\
      break;							\
    case DOUB:							\
      GEN_LOd;							\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_LOld;							\
      break;							\
    case FLT:							\
      GEN_LOf;							\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }

#define GEN_GRi ((struct OPERAND_0_mi *)kodp)->major = GR;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_GRui ((struct OPERAND_0_mi *)kodp)->major = GR;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_GRli ((struct OPERAND_0_mi *)kodp)->major = GR;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_GRlui ((struct OPERAND_0_mi *)kodp)->major = GR;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_GRd ((struct OPERAND_0_mi *)kodp)->major = GR;\
((struct OPERAND_0_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_GRld ((struct OPERAND_0_mi *)kodp)->major = GR;\
((struct OPERAND_0_mi *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_GRf ((struct OPERAND_0_mi *)kodp)->major = GR;\
((struct OPERAND_0_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_GRc ((struct OPERAND_0_mi *)kodp)->major = GR;\
((struct OPERAND_0_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_GR							\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_GRi;							\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_GRui;							\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_GRli;							\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_GRlui;						\
      break;							\
    case DOUB:							\
      GEN_GRd;							\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_GRld;							\
      break;							\
    case FLT:							\
      GEN_GRf;							\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }

#define GEN_LEi ((struct OPERAND_0_mi *)kodp)->major = LE;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_LEui ((struct OPERAND_0_mi *)kodp)->major = LE;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_LEli ((struct OPERAND_0_mi *)kodp)->major = LE;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_LElui ((struct OPERAND_0_mi *)kodp)->major = LE;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_LEd ((struct OPERAND_0_mi *)kodp)->major = LE;\
((struct OPERAND_0_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_LEld ((struct OPERAND_0_mi *)kodp)->major = LE;\
((struct OPERAND_0_mi *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_LEf ((struct OPERAND_0_mi *)kodp)->major = LE;\
((struct OPERAND_0_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_LEc ((struct OPERAND_0_mi *)kodp)->major = LE;\
((struct OPERAND_0_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_LE							\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_LEi;							\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_LEui;							\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_LEli;							\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_LElui;						\
      break;							\
    case DOUB:							\
      GEN_LEd;							\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_LEld;							\
      break;							\
    case FLT:							\
      GEN_LEf;							\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }

#define GEN_GEi ((struct OPERAND_0_mi *)kodp)->major = GE;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_GEui ((struct OPERAND_0_mi *)kodp)->major = GE;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_GEli ((struct OPERAND_0_mi *)kodp)->major = GE;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_GElui ((struct OPERAND_0_mi *)kodp)->major = GE;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_GEd ((struct OPERAND_0_mi *)kodp)->major = GE;\
((struct OPERAND_0_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_GEld ((struct OPERAND_0_mi *)kodp)->major = GE;\
((struct OPERAND_0_mi *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_GEf ((struct OPERAND_0_mi *)kodp)->major = GE;\
((struct OPERAND_0_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_GEc ((struct OPERAND_0_mi *)kodp)->major = GE;\
((struct OPERAND_0_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_GE							\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_GEi;							\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_GEui;							\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_GEli;							\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_GElui;						\
      break;							\
    case DOUB:							\
      GEN_GEd;							\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_GEld;							\
      break;							\
    case FLT:							\
      GEN_GEf;							\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }

#define GEN_NEi ((struct OPERAND_0_mi *)kodp)->major = NE;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_NEui ((struct OPERAND_0_mi *)kodp)->major = NE;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_NEli ((struct OPERAND_0_mi *)kodp)->major = NE;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_NElui ((struct OPERAND_0_mi *)kodp)->major = NE;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_NEd ((struct OPERAND_0_mi *)kodp)->major = NE;\
((struct OPERAND_0_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_NEld ((struct OPERAND_0_mi *)kodp)->major = NE;\
((struct OPERAND_0_mi *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_NEf ((struct OPERAND_0_mi *)kodp)->major = NE;\
((struct OPERAND_0_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_NEc ((struct OPERAND_0_mi *)kodp)->major = NE;\
((struct OPERAND_0_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_NE							\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_NEi;							\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_NEui;							\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_NEli;							\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_NElui;						\
      break;							\
    case DOUB:							\
      GEN_NEd;							\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_NEld;							\
      break;							\
    case FLT:							\
      GEN_NEf;							\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }


#define GEN_NEGi ((struct OPERAND_0_mi *)kodp)->major = NEG;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_NEGui ((struct OPERAND_0_mi *)kodp)->major = NEG;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_NEGli ((struct OPERAND_0_mi *)kodp)->major = NEG;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_NEGlui ((struct OPERAND_0_mi *)kodp)->major = NEG;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_NEGd ((struct OPERAND_0_mi *)kodp)->major = NEG;\
((struct OPERAND_0_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_NEGld ((struct OPERAND_0_mi *)kodp)->major = NEG;\
((struct OPERAND_0_mi *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_NEGf ((struct OPERAND_0_mi *)kodp)->major = NEG;\
((struct OPERAND_0_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);

#if 0
#define GEN_NEGc ((struct OPERAND_0_mi *)kodp)->major = NEG;\
((struct OPERAND_0_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_NEG							\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_NEGi;							\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_NEGui;						\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_NEGli;						\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_NEGlui;						\
      break;							\
    case DOUB:							\
      GEN_NEGd;							\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_NEGld;						\
      break;							\
    case FLT:							\
      GEN_NEGf;							\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }

#define GEN_NOT ((struct OPERAND_0_ma *)kodp)->major = NOT;\
kodp += sizeof (struct OPERAND_0_ma);

#define GEN_SAL ((struct OPERAND_0_ma *)kodp)->major = SAL;\
kodp += sizeof (struct OPERAND_0_ma);

#define GEN_SAR ((struct OPERAND_0_ma *)kodp)->major = SAR;\
kodp += sizeof (struct OPERAND_0_ma);

#define GEN_XOR ((struct OPERAND_0_ma *)kodp)->major = XOR;\
kodp += sizeof (struct OPERAND_0_ma);



              /* 
	       * I/O instructions. 
	       */

#define GEN_INi(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = IN;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_INri(off1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INrri(of1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INoi(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = IN;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_INroi(off1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 4;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INrroi(of1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 5;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INd(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = IN;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_INrd(off1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INrrd(of1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 258;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INod(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = IN;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 259;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_INrod(off1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 260;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INrrod(of1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 261;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INf(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = IN;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_INrf(off1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 513;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INrrf(of1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 514;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INof(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = IN;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 515;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_INrof(off1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 516;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INrrof(of1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 517;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INc(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = IN;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_INrc(off1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 769;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INrrc(of1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 770;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INoc(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = IN;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 771;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_INroc(off1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 772;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_INrroc(of1) ((struct OPERAND_1_i *)kodp)->major = IN;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 773;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUTi(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = OUT;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_OUTri(off1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUTrri(of1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUToi(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = OUT;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_OUTroi(off1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 4;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUTrroi(of1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 5;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUTd(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = OUT;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_OUTrd(off1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUTrrd(of1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 258;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUTod(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = OUT;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 259;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_OUTrod(off1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 260;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUTrrod(of1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 261;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUTf(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = OUT;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_OUTrf(off1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 513;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUTrrf(of1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 514;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUTof(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = OUT;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 515;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_OUTrof(off1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 516;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUTrrof(of1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 517;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUTc(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = OUT;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_OUTrc(off1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 769;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUTrrc(of1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 770;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUToc(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = OUT;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
((struct OPERAND_1_mi *)kodp)->minor = 771;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_OUTroc(off1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = off1;\
((struct OPERAND_1_i *)kodp)->minor = 772;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_OUTrroc(of1) ((struct OPERAND_1_i *)kodp)->major = OUT;\
((struct OPERAND_1_i *)kodp)->num = of1;\
((struct OPERAND_1_i *)kodp)->minor = 773;\
kodp += sizeof (struct OPERAND_1_i);



              /* 
	       * Instructions on stacks.
	       */

#define GEN_PUSHA(v_adr) ((struct OPERAND_1_mi *)kodp)->major = PUSHA;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr;\
((struct OPERAND_1_mi *)kodp)->minor = 0;\
kodp +=  sizeof (struct OPERAND_1_mi);

#define GEN_PUSHAr(off) ((struct OPERAND_1_i *)kodp)->major = PUSHA;\
((struct OPERAND_1_i *)kodp)->num = off;\
((struct OPERAND_1_i *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_PUSHArr(off) ((struct OPERAND_1_i *)kodp)->major = PUSHA;\
((struct OPERAND_1_i *)kodp)->num = off;\
((struct OPERAND_1_i *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_PUSHAIi(n) ((struct OPERAND_1_i *)kodp)->major = PUSHAI;\
((struct OPERAND_1_i *)kodp)->minor = 0;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_PUSHAIui(n) ((struct OPERAND_1_ui *)kodp)->major = PUSHAI;\
((struct OPERAND_1_ui *)kodp)->minor = 1;\
((struct OPERAND_1_ui *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_ui);

#define GEN_PUSHAIli(n) ((struct OPERAND_1_li *)kodp)->major = PUSHAI;\
((struct OPERAND_1_li *)kodp)->minor = 2;\
((struct OPERAND_1_li *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_li);

#define GEN_PUSHAIlui(n) ((struct OPERAND_1_lui *)kodp)->major = PUSHAI;\
((struct OPERAND_1_lui *)kodp)->minor = 3;\
((struct OPERAND_1_lui *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_lui);

#define GEN_PUSHAIsi(n) ((struct OPERAND_1_si *)kodp)->major = PUSHAI;\
((struct OPERAND_1_si *)kodp)->minor = 4;\
((struct OPERAND_1_si *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_si);\
align_memory (&kodp, sizeof(int));

#define GEN_PUSHAIsui(n) ((struct OPERAND_1_sui *)kodp)->major = PUSHAI;\
((struct OPERAND_1_sui *)kodp)->minor = 5;\
((struct OPERAND_1_sui *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_sui);\
align_memory (&kodp, sizeof(int));

#define GEN_PUSHAId(n) ((struct OPERAND_1_id *)kodp)->major = PUSHAI;\
((struct OPERAND_1_id *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);\
align_memory (&kodp, sizeof(double));\
*(double *)kodp = n;\
kodp += sizeof (double);

#define GEN_PUSHAIld(n) ((struct OPERAND_1_ild *)kodp)->major = PUSHAI;\
((struct OPERAND_1_ild *)kodp)->minor = 257;\
kodp += sizeof (struct OPERAND_0_mi);\
align_memory (&kodp, sizeof (long double));\
*(long double *)kodp = n;\
kodp += sizeof (long double);

#define GEN_PUSHAIf(n) ((struct OPERAND_1_if *)kodp)->major = PUSHAI;\
((struct OPERAND_1_if *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);\
align_memory (&kodp, sizeof(float));\
*(float *)kodp = n;\
kodp += sizeof (float);

#define GEN_PUSHAIc(n) ((struct OPERAND_1_ic *)kodp)->major = PUSHAI;\
((struct OPERAND_1_ic *)kodp)->minor = 768;\
((struct OPERAND_1_ic *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_ic);\
align_memory (&kodp, sizeof(int));

#define GEN_PUSHAIst(n) ((struct OPERAND_1_mi *)kodp)->major = PUSHAI;\
((struct OPERAND_1_mi *)kodp)->minor = 769;\
((struct OPERAND_1_mi *)kodp)->adr = n;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_PUSHAIsc(n) ((struct OPERAND_1_isc *)kodp)->major = PUSHAI;\
((struct OPERAND_1_isc *)kodp)->minor = 770;\
((struct OPERAND_1_isc *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_isc);\
align_memory (&kodp, sizeof (int));

#define GEN_PUSHAIuc(n) ((struct OPERAND_1_iuc *)kodp)->major = PUSHAI;\
((struct OPERAND_1_iuc *)kodp)->minor = 771;\
((struct OPERAND_1_iuc *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_iuc);\
align_memory (&kodp, sizeof (int));

#define GEN_PUSHAI(num)						\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_PUSHAIi(num);						\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_PUSHAIui((unsigned int) num);				\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_PUSHAIli((long int) num);				\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_PUSHAIlui((long unsigned int) num);			\
      break;							\
    case SHORT_AC | INTEGER:					\
    case SHORT_AC | SIGNED_AC | INTEGER:			\
      GEN_PUSHAIsi((short int) num);				\
      break;							\
    case SHORT_AC | UNSIGNED_AC | INTEGER:			\
      GEN_PUSHAIsui((short unsigned int) num);			\
      break;							\
    case DOUB:							\
      GEN_PUSHAId((double) num);				\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_PUSHAIld((long double) num);				\
      break;							\
    case FLT:							\
      GEN_PUSHAIf((float) num);					\
      break;							\
    case CHR:							\
      GEN_PUSHAIc((char) num);					\
      break;							\
    case SIGNED_AC | CHR:					\
      GEN_PUSHAIsc((signed char) num);				\
      break;							\
    case UNSIGNED_AC | CHR:					\
      GEN_PUSHAIc((unsigned char) num);				\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }

#define GEN_POPAe ((struct OPERAND_0_mi *)kodp)->major = POPA;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_POPAst(n) ((struct OPERAND_1_i *)kodp)->major = POPA;\
((struct OPERAND_1_i *)kodp)->minor = 1;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_POPAi(n) ((struct OPERAND_1_i *)kodp)->major = POPA;\
((struct OPERAND_1_i *)kodp)->minor = 2;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_POPAui(n) ((struct OPERAND_1_i *)kodp)->major = POPA;\
((struct OPERAND_1_i *)kodp)->minor = 3;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_POPAli(n) ((struct OPERAND_1_i *)kodp)->major = POPA;\
((struct OPERAND_1_i *)kodp)->minor = 4;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_POPAlui(n) ((struct OPERAND_1_i *)kodp)->major = POPA;\
((struct OPERAND_1_i *)kodp)->minor = 5;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_POPAsi(n) ((struct OPERAND_1_i *)kodp)->major = POPA;\
((struct OPERAND_1_i *)kodp)->minor = 6;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_POPAsui(n) ((struct OPERAND_1_i *)kodp)->major = POPA;\
((struct OPERAND_1_i *)kodp)->minor = 7;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_POPAd(n) ((struct OPERAND_1_i *)kodp)->major = POPA;\
((struct OPERAND_1_i *)kodp)->minor = 256;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_POPAld(n) ((struct OPERAND_1_i *)kodp)->major = POPA;\
((struct OPERAND_1_i *)kodp)->minor = 257;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_POPAf(n) ((struct OPERAND_1_i *)kodp)->major = POPA;\
((struct OPERAND_1_i *)kodp)->minor = 512;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_POPAc(n) ((struct OPERAND_1_i *)kodp)->major = POPA;\
((struct OPERAND_1_i *)kodp)->minor = 768;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_POPAsc(n) ((struct OPERAND_1_i *)kodp)->major = POPA;\
((struct OPERAND_1_i *)kodp)->minor = 769;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_POPAuc(n) ((struct OPERAND_1_i *)kodp)->major = POPA;\
((struct OPERAND_1_i *)kodp)->minor = 770;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_POPAb(n) ((struct OPERAND_1_i *)kodp)->major = POPA;\
((struct OPERAND_1_i *)kodp)->minor = 771;\
((struct OPERAND_1_i *)kodp)->num = n;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_POPA(off)						\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_POPAi(off);						\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_POPAui(off);						\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_POPAli(off);						\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_POPAlui(off);						\
      break;							\
    case SHORT_AC | INTEGER:					\
    case SHORT_AC | SIGNED_AC | INTEGER:			\
      GEN_POPAsi(off);						\
      break;							\
    case SHORT_AC | UNSIGNED_AC | INTEGER:			\
      GEN_POPAsui(off);						\
      break;							\
    case DOUB:							\
      GEN_POPAd(off);						\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_POPAld(off);						\
      break;							\
    case FLT:							\
      GEN_POPAf(off);						\
      break;							\
    case CHR:							\
      GEN_POPAc(off);						\
      break;							\
    case SIGNED_AC | CHR:					\
      GEN_POPAsc(off);						\
      break;							\
    case UNSIGNED_AC | CHR:					\
      GEN_POPAuc(off);						\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }

#define GEN_PUSHb ((struct OPERAND_0_mi *)kodp)->major = PUSH;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_PUSHt ((struct OPERAND_0_mi *)kodp)->major = PUSH;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_PUSHf ((struct OPERAND_0_mi *)kodp)->major = PUSH;\
((struct OPERAND_0_mi *)kodp)->minor = 4;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_POPb ((struct OPERAND_0_mi *)kodp)->major = POP;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_POPt ((struct OPERAND_0_mi *)kodp)->major = POP;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_POPf ((struct OPERAND_0_mi *)kodp)->major = POP;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);



                 /*
                  * Address instructions.
                  */

#define GEN_MOVi ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVbs ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVsb ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVt ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVfs ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 4;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVsf ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 5;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVaa ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 6;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVap ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 7;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVpa ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 8;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVar ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 9;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVvi ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 10;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVbv ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 11;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVui ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 12;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVli ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 13;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVlui ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 14;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVsi ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 15;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVsui ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 16;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVvui ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 17;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVvli ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 18;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVvlui ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 19;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVvsi ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 20;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVvsui ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 21;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVd ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 256;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVvd ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 257 ;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVld ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 258;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVvld ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 259;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVf ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 512;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVvf ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 513;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVc ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 768;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVvc ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 769;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVsc ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 770;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVuc ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 771;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVvsc ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 772;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOVvuc ((struct OPERAND_0_mi *)kodp)->major = MOV;\
((struct OPERAND_0_mi *)kodp)->minor = 773;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_MOV							\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_MOVi;							\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_MOVui;						\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_MOVli;						\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_MOVlui;						\
      break;							\
    case SHORT_AC | INTEGER:					\
    case SHORT_AC | SIGNED_AC | INTEGER:			\
      GEN_MOVsi;						\
      break;							\
    case SHORT_AC | UNSIGNED_AC | INTEGER:			\
      GEN_MOVsui;						\
      break;							\
    case DOUB:							\
      GEN_MOVd;							\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_MOVld;						\
      break;							\
    case FLT:							\
      GEN_MOVf;							\
      break;							\
    case CHR:							\
      GEN_MOVc;							\
      break;							\
    case SIGNED_AC | CHR:					\
      GEN_MOVsc;						\
      break;							\
    case UNSIGNED_AC | CHR:					\
      GEN_MOVuc;						\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }

#define GEN_MOVv						\
  switch (type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      GEN_MOVvi;						\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      GEN_MOVvui;						\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      GEN_MOVvli;						\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      GEN_MOVvlui;						\
      break;							\
    case SHORT_AC | INTEGER:					\
    case SHORT_AC | SIGNED_AC | INTEGER:			\
      GEN_MOVvsi;						\
      break;							\
    case SHORT_AC | UNSIGNED_AC | INTEGER:			\
      GEN_MOVvsui;						\
      break;							\
    case DOUB:							\
      GEN_MOVvd;						\
      break;							\
    case LONG_AC | DOUB:					\
      GEN_MOVvld;						\
      break;							\
    case FLT:							\
      GEN_MOVvf;						\
      break;							\
    case CHR:							\
      GEN_MOVvc;						\
      break;							\
    case SIGNED_AC | CHR:					\
      GEN_MOVvsc;						\
      break;							\
    case UNSIGNED_AC | CHR:					\
      GEN_MOVvuc;						\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }



                  /* 
		   * Instructions on the stack of temporaries.
		   * Instructions for casts of operands.
		   */

#define GEN_CLRT ((struct OPERAND_0_ma *)kodp)->major = CLRT;\
kodp += sizeof (struct OPERAND_0_ma);

#define GEN_CLRTr ((struct OPERAND_0_ma *)kodp3)->major = CLRT;\
kodp += sizeof (struct OPERAND_0_ma);

#define GEN_CVTIDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 0;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTIDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 1;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTDI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 2;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTIFt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 3;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTIFb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 4;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTFI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 5;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTFDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 6;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTFDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 7;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTDF ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 8;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTCIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 9;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTCIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 10;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTIC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 11;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTCDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 12;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTCDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 13;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTDC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 14;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTCFt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 15;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTCFb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 16;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTFC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 17;\
kodp += sizeof (struct OPERAND_0_mi);
				/*  */
#define GEN_CVTUIDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 18;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUIDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 19;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUIFt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 20;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUIFb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 21;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUILIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 22;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUILIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 23;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef UINT_EQ_ULONG
#define GEN_CVTUILUIt
#define GEN_CVTUILUIb
#else
#define GEN_CVTUILUIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 24;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUILUIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 25;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTUII ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 26;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef SHORT_EQ_INT
#define GEN_CVTUISI GEN_CVTUII
#else
#define GEN_CVTUISI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 27;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_USHORT
#define GEN_CVTUISUI
#else
#define GEN_CVTUISUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 28;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTUIC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 29;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUISC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 30;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUIUC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 31;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef LONG_EQ_INT

#define GEN_CVTLIDt GEN_CVTIDt
#define GEN_CVTLIDb GEN_CVTIDb
#define GEN_CVTLIFt GEN_CVTIFt
#define GEN_CVTLIFb GEN_CVTIFb
#define GEN_CVTLILUIt GEN_CVTILUIt
#define GEN_CVTLILUIb GEN_CVTILUIb
#define GEN_CVTLII
#define GEN_CVTLIUI GEN_CVTIUIt

#ifdef SHORT_EQ_INT
#define GEN_CVTLISI
#else
#define GEN_CVTLISI GEN_CVTISI
#endif

#define GEN_CVTLISUI GEN_CVTISUI
#define GEN_CVTLIC GEN_CVTIC
#define GEN_CVTLISC GEN_CVTISC
#define GEN_CVTLIUC GEN_CVTIUC

#else
#define GEN_CVTLIDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 32;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLIDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 33;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLIFt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 34;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLIFb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 35;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLILUIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 36;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLILUIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 37;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLII ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 38;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLIUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 39;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef SHORT_EQ_INT
#define GEN_CVTLISI GEN_CVTLII
#else
#define GEN_CVTLISI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 40;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTLISUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 41;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLIC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 42;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLISC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 43;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLIUC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 44;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_ULONG
#define GEN_CVTLUIDt GEN_CVTUIDt
#define GEN_CVTLUIDb GEN_CVTUIDb
#define GEN_CVTLUIFt GEN_CVTUIFt
#define GEN_CVTLUIFb GEN_CVTUIFb
#define GEN_CVTLUILI GEN_CVTUILIt
#define GEN_CVTLUIUI
#define GEN_CVTLUII GEN_CVTUII
#define GEN_CVTLUISUI GEN_CVTUISUI
#define GEN_CVTLUISI GEN_CVTUISI
#define GEN_CVTLUIC GEN_CVTUIC
#define GEN_CVTLUISC GEN_CVTUISC
#define GEN_CVTLUIUC GEN_CVTUIUC

#else

#define GEN_CVTLUIDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 45;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLUIDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 46;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLUIFt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 47;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLUIFb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 48;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLUILI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 49;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLUIUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 50;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLUII ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 51;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLUISUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 52;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLUISI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 53;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLUIC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 54;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLUISC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 55;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLUIUC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 56;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef SHORT_EQ_INT
#define GEN_CVTSIDt GEN_CVTIDt
#define GEN_CVTSIDb GEN_CVTIDb
#define GEN_CVTSIFt GEN_CVTIFt
#define GEN_CVTSIFb GEN_CVTIFb
#define GEN_CVTSILIt GEN_CVTILIt
#define GEN_CVTSILIb GEN_CVTILIb
#define GEN_CVTSILUIt GEN_CVTILUIt
#define GEN_CVTSILUIb GEN_CVTILUIb
#define GEN_CVTSIIt
#define GEN_CVTSIIb
#define GEN_CVTSIUIt GEN_CVTIUIt
#define GEN_CVTSIUIb GEN_CVTIUIb
#define GEN_CVTSISUI GEN_CVTISUI
#define GEN_CVTSIC GEN_CVTIC
#define GEN_CVTSIUC GEN_CVTIUC
#define GEN_CVTSISC GEN_CVTISC

#else

#define GEN_CVTSIDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 57;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSIDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 58;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSIFt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 59;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSIFb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 60;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef LONG_EQ_INT
#define GEN_CVTSILIt GEN_CVTSIIt
#define GEN_CVTSILIb GEN_CVTSIIb
#else
#define GEN_CVTSILIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 61;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSILIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 62;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTSILUIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 63;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSILUIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 64;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSIIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 65;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSIIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 66;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSIUIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 67;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSIUIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 68;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSISUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 69;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSIC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 70;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSIUC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 71;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSISC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 72;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_USHORT
#define GEN_CVTSUIDt GEN_CVTUIDt
#define GEN_CVTSUIDb GEN_CVTUIDb
#define GEN_CVTSUIFt GEN_CVTUIFt
#define GEN_CVTSUIFb GEN_CVTUIFb
#define GEN_CVTSUILIt GEN_CVTUILIt
#define GEN_CVTSUILIb GEN_CVTUILIb
#define GEN_CVTSUILUIt GEN_CVTUILUIt
#define GEN_CVTSUILUIb GEN_CVTUILUIb
#define GEN_CVTSUIIt GEN_CVTUIIt
#define GEN_CVTSUIIb GEN_CVTUIIb
#define GEN_CVTSUIUIt
#define GEN_CVTSUIUIb
#define GEN_CVTSUISI GEN_CVTUISI
#define GEN_CVTSUIC GEN_CVTUIC
#define GEN_CVTSUIUC GEN_CVTUIUC
#define GEN_CVTSUISC GEN_CVTUISC

#else
#define GEN_CVTSUIDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 73;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUIDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 74;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUIFt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 75;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUIFb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 76;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUILIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 77;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUILIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 78;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUILUIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 79;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUILUIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 80;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUIIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 81;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUIIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 82;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUIUIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 83;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUIUIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 84;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUISI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 85;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUIC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 86;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUIUC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 87;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUISC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 88;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef LONG_EQ_INT
#define GEN_CVTCLIt GEN_CVTCIt
#define GEN_CVTCLIb GEN_CVTCIb
#else
#define GEN_CVTCLIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 89;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTCLIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 90;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_ULONG
#define GEN_CVTCLUIt GEN_CVTCUIt
#define GEN_CVTCLUIb GEN_CVTCUIb
#else
#define GEN_CVTCLUIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 91;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTCLUIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 92;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTCUIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 93;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTCUIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 94;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef SHORT_EQ_INT
#define GEN_CVTCSI GEN_CVTCI
#else
#define GEN_CVTCSI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 95;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_USHORT
#define GEN_CVTCSUI GEN_CVTCUIt
#else
#define GEN_CVTCSUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 96;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTCSC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 97;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTCUC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 98;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSCDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 99;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSCDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 100;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSCFt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 101;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSCFb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 102;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef LONG_EQ_INT
#define GEN_CVTSCLIt GEN_CVTSCIt
#define GEN_CVTSCLIb GEN_CVTSCIb
#else
#define GEN_CVTSCLIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 103;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSCLIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 104;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_ULONG
#define GEN_CVTSCLUIt GEN_CVTSCUIt
#define GEN_CVTSCLUIb GEN_CVTSCUIb
#else
#define GEN_CVTSCLUIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 105;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSCLUIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 106;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTSCIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 107;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSCIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 108;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSCUIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 109;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSCUIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 110;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef SHORT_EQ_INT
#define GEN_CVTSCSI GEN_CVTSCIt
#else
#define GEN_CVTSCSI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 111;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_USHORT
#define GEN_CVTSCSUI GEN_CVTSCUIt
#else
#define GEN_CVTSCSUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 112;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTSCC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 113;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSCUC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 114;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUCDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 115;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUCDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 116;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUCFt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 117;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUCFb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 118;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef LONG_EQ_INT
#define GEN_CVTUCLIt GEN_CVTUCIt
#define GEN_CVTUCLIb GEN_CVTUCIb
#else
#define GEN_CVTUCLIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 119;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUCLIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 120;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_ULONG
#define GEN_CVTUCLUIt GEN_CVTUCUIt
#define GEN_CVTUCLUIb GEN_CVTUCUIb
#else
#define GEN_CVTUCLUIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 121;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUCLUIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 122;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTUCIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 123;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUCIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 124;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUCUIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 125;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUCUIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 126;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef SHORT_EQ_INT
#define GEN_CVTUCSI GEN_CVTUCIt
#else
#define GEN_CVTUCSI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 127;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_USHORT
#define GEN_CVTUCSUI GEN_CVTUCUIt
#else
#define GEN_CVTUCSUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 128;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTUCC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 129;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUCSC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 130;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef LONG_EQ_INT
#define GEN_CVTDLI GEN_CVTDI
#else
#define GEN_CVTDLI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 131;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_ULONG
#define GEN_CVTDLUI GEN_CVTDUI
#else
#define GEN_CVTDLUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 132;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTDUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 133;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef SHORT_EQ_INT
#define GEN_CVTDSI GEN_CVTDI
#else
#define GEN_CVTDSI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 134;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_USHORT
#define GEN_CVTDSUI GEN_CVTDUI
#else
#define GEN_CVTDSUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 135;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTDSC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 136;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTDUC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 137;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef LONG_EQ_INT
#define GEN_CVTFLI GEN_CVTFI
#else
#define GEN_CVTFLI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 138;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_ULONG
#define GEN_CVTFLUI GEN_CVTFUI
#else
#define GEN_CVTFLUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 139;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTFUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 140;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef SHORT_EQ_INT
#define GEN_CVTFSI GEN_CVTFI
#else
#define GEN_CVTFSI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 141;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_USHORT
#define GEN_CVTFSUI GEN_CVTFUI
#else
#define GEN_CVTFSUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 142;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTFSC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 143;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTFUC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 144;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef LONG_EQ_INT
#define GEN_CVTILIt
#define GEN_CVTILIb
#else
#define GEN_CVTILIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 145;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTILIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 146;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_ULONG
#define GEN_CVTILUIt GEN_CVTIUIt
#define GEN_CVTILUIb GEN_CVTIUIb
#else
#define GEN_CVTILUIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 147;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTILUIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 148;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTIUIt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 149;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTIUIb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 150;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef SHORT_EQ_INT
#define GEN_CVTISI
#else
#define GEN_CVTISI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 151;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_USHORT
#define GEN_CVTISUI GEN_CVTIUIt
#else
#define GEN_CVTISUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 152;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTISC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 153;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTIUC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 154;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef LDOUBLE_EQ_DOUBLE

#define GEN_CVTLDD
#define GEN_CVTLDF GEN_CVTDF
#define GEN_CVTLDLI GEN_CVTDLI
#define GEN_CVTLDLUI GEN_CVTDLUI
#define GEN_CVTLDI GEN_CVTDI
#define GEN_CVTLDUI GEN_CVTDUI
#define GEN_CVTLDSI GEN_CVTDSI
#define GEN_CVTLDSUI GEN_CVTDSUI
#define GEN_CVTLDC GEN_CVTDC
#define GEN_CVTLDUC GEN_CVTDUC
#define GEN_CVTLDSC GEN_CVTDSC
#define GEN_CVTILDt GEN_CVTIDt
#define GEN_CVTILDb GEN_CVTIDb
#define GEN_CVTUILDt GEN_CVTUIDt
#define GEN_CVTUILDb GEN_CVTUIDb
#define GEN_CVTLILDt GEN_CVTLIDt
#define GEN_CVTLILDb GEN_CVTLIDb
#define GEN_CVTSILDt GEN_CVTSIDt
#define GEN_CVTSILDb GEN_CVTSIDb
#define GEN_CVTLUILDt GEN_CVTLUIDt
#define GEN_CVTLUILDb GEN_CVTLUIDb
#define GEN_CVTSUILDt GEN_CVTSUIDt
#define GEN_CVTSUILDb GEN_CVTSUIDb
#define GEN_CVTDLDt
#define GEN_CVTDLDb
#define GEN_CVTFLDt GEN_CVTFDt
#define GEN_CVTFLDb GEN_CVTFDb
#define GEN_CVTCLDt GEN_CVTCDt
#define GEN_CVTCLDb GEN_CVTCDb
#define GEN_CVTSCLDt GEN_CVTSCDt
#define GEN_CVTSCLDb GEN_CVTSCDb
#define GEN_CVTUCLDt GEN_CVTUCDt
#define GEN_CVTUCLDb GEN_CVTUCDb

#else
#define GEN_CVTLDD ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 155;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLDF ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 156;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef LONG_EQ_INT
#define GEN_CVTLDLI GEN_CVTLDI
#else
#define GEN_CVTLDLI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 157;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_ULONG
#define GEN_CVTLDLUI GEN_CVTLDUI
#else
#define GEN_CVTLDLUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 158;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTLDI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 159;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLDUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 160;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef SHORT_EQ_INT
#define GEN_CVTLDSI GEN_CVTLDI
#else
#define GEN_CVTLDSI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 161;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_USHORT
#define GEN_CVTLDSUI GEN_CVTLDUI
#else
#define GEN_CVTLDSUI ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 162;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTLDC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 163;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLDUC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 164;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLDSC ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 165;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTILDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 166;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTILDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 167;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUILDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 168;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUILDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 169;\
kodp += sizeof (struct OPERAND_0_mi);

#ifdef LONG_EQ_INT
#define GEN_CVTLILDt GEN_CVTILDt
#define GEN_CVTLILDb GEN_CVTILDb
#else
#define GEN_CVTLILDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 170;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLILDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 171;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef SHORT_EQ_INT
#define GEN_CVTSILDt GEN_CVTILDt
#define GEN_CVTSILDb GEN_CVTILDb
#else
#define GEN_CVTSILDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 172;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSILDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 173;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_ULONG
#define GEN_CVTLUILDt GEN_CVTUILDt
#define GEN_CVTLUILDb GEN_CVTUILDb
#else
#define GEN_CVTLUILDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 174;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTLUILDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 175;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#ifdef UINT_EQ_USHORT
#define GEN_CVTSUILDt GEN_CVTUILDt
#define GEN_CVTSUILDb GEN_CVTUILDb
#else
#define GEN_CVTSUILDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 176;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSUILDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 177;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_CVTDLDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 178;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTDLDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 179;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTFLDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 180;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTFLDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 181;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTCLDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 182;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTCLDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 183;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSCLDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 184;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTSCLDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 185;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUCLDt ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 186;\
kodp += sizeof (struct OPERAND_0_mi);

#define GEN_CVTUCLDb ((struct OPERAND_0_mi *)kodp)->major = CVT;\
((struct OPERAND_0_mi *)kodp)->minor = 187;\
kodp += sizeof (struct OPERAND_0_mi);
#endif

#define GEN_MESS(mes) ((struct OPERAND_1_ma *)kodp)->major = MESS;\
((struct OPERAND_1_ma *)kodp)->adr = mes;\
kodp += sizeof (struct OPERAND_1_ma);



                    /* 
		     * Control instructions.
		     */

#define GEN_STOP ((struct OPERAND_0_ma *)kodp)->major = STOP;\
kodp += sizeof (struct OPERAND_0_ma);

#define GEN_INTER ((struct OPERAND_0_ma *)kodp)->major = INTER;\
kodp += sizeof (struct OPERAND_0_ma);

#define GEN_IRET ((struct OPERAND_0_ma *)kodp)->major = IRET;\
kodp += sizeof (struct OPERAND_0_ma);

#define GEN_HALT ((struct OPERAND_0_ma *)kodp)->major = HALT;\
kodp += sizeof (struct OPERAND_0_ma);


                    /* 
		     * Jump instructions.
		     */

#define GEN_JMP ((struct OPERAND_1_ma *)kodp)->major = JMP;\
kodp += sizeof (struct OPERAND_1_ma);

#define GEN_JZ ((struct OPERAND_1_ma *)kodp)->major = JZ;\
kodp += sizeof (struct OPERAND_1_ma);

#define GEN_JNZ ((struct OPERAND_1_ma *)kodp)->major = JNZ;\
kodp += sizeof (struct OPERAND_1_ma);


                    /*
		     * Instructions call and ret.
		     */

#define GEN_CALL(v_adr1) ((struct OPERAND_1_mi *)kodp)->major = CALLi;\
((struct OPERAND_1_mi *)kodp)->minor = 0;\
((struct OPERAND_1_mi *)kodp)->adr = v_adr1;\
kodp += sizeof (struct OPERAND_1_mi);

#define GEN_XCALLi(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 1;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLd(off1) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 2;\
((struct OPERAND_1_i *)kodp)->num = off1;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLf(off1) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 3;\
((struct OPERAND_1_i *)kodp)->num = off1;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLc(off1) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 4;\
((struct OPERAND_1_i *)kodp)->num = off1;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLv(off1) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 5;\
((struct OPERAND_1_i *)kodp)->num = off1;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLui(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 6;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLli(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 7;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLlui(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 8;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLsi(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 9;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLsui(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 10;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLld(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 11;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLsc(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 12;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLuc(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 13;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLiv(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 14;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLdv(off1) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 15;\
((struct OPERAND_1_i *)kodp)->num = off1;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLfv(off1) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 16;\
((struct OPERAND_1_i *)kodp)->num = off1;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLcv(off1) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 17;\
((struct OPERAND_1_i *)kodp)->num = off1;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLvv(off1) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 18;\
((struct OPERAND_1_i *)kodp)->num = off1;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLuiv(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 19;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLliv(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 20;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLluiv(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 21;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLsiv(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 22;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLsuiv(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 23;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLldv(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 24;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLscv(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 25;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALLucv(off) ((struct OPERAND_1_i *)kodp)->major = CALLi;\
((struct OPERAND_1_i *)kodp)->minor = 26;\
((struct OPERAND_1_i *)kodp)->num = off;\
kodp += sizeof (struct OPERAND_1_i);

#define GEN_XCALL						\
  switch(type_ac[set])						\
    {								\
    case INTEGER:						\
    case SIGNED_AC | INTEGER:					\
      if (call_by_value)					\
        {							\
	  GEN_XCALLiv(*(int *)proc_name[proc]);			\
	}							\
      else if (call_by_reference)				\
	{							\
	  GEN_XCALLi(*(int *)proc_name[proc]);			\
	}							\
      break;							\
    case UNSIGNED_AC | INTEGER:					\
      if (call_by_value)					\
	{							\
	  GEN_XCALLuiv(*(int *)proc_name[proc]);		\
	}							\
      else if (call_by_reference)				\
	{							\
	  GEN_XCALLui(*(int *)proc_name[proc]);			\
	}							\
      break;							\
    case LONG_AC | INTEGER:					\
    case LONG_AC | SIGNED_AC | INTEGER:				\
      if (call_by_value)					\
	{							\
	  GEN_XCALLliv(*(int *)proc_name[proc]);		\
	}							\
      else if (call_by_reference)				\
	{							\
	  GEN_XCALLli(*(int *)proc_name[proc]);			\
	}							\
      break;							\
    case LONG_AC | UNSIGNED_AC | INTEGER:			\
      if (call_by_value)					\
	{							\
	  GEN_XCALLluiv(*(int *)proc_name[proc]);		\
	}							\
      else if (call_by_reference)				\
	{							\
	  GEN_XCALLlui(*(int *)proc_name[proc]);		\
	}							\
      break;							\
    case SHORT_AC | INTEGER:					\
    case SHORT_AC | SIGNED_AC | INTEGER:			\
      if (call_by_value)					\
	{							\
	  GEN_XCALLsiv(*(int *)proc_name[proc]);		\
	}							\
      else if (call_by_reference)				\
	{							\
	  GEN_XCALLsi(*(int *)proc_name[proc]);			\
	}							\
      break;							\
    case SHORT_AC | UNSIGNED_AC | INTEGER:			\
      if (call_by_value)					\
	{							\
	  GEN_XCALLsuiv(*(int *)proc_name[proc]);		\
	}							\
      else if (call_by_reference)				\
	{							\
	  GEN_XCALLsui(*(int *)proc_name[proc]);		\
	}							\
      break;							\
    case DOUB:							\
      if (call_by_value)					\
	{							\
	  GEN_XCALLdv(*(int *)proc_name[proc]);			\
	}							\
      else if (call_by_reference)				\
	{							\
	  GEN_XCALLd(*(int *)proc_name[proc]);			\
	}							\
      break;							\
    case LONG_AC | DOUB:					\
      if (call_by_value)					\
	{							\
	  GEN_XCALLldv(*(int *)proc_name[proc]);		\
	}							\
      else if (call_by_reference)				\
	{							\
	  GEN_XCALLld(*(int *)proc_name[proc]);			\
	}							\
      break;							\
    case FLT:							\
      if (call_by_value)					\
	{							\
	  GEN_XCALLfv(*(int *)proc_name[proc]);			\
	}							\
      else if (call_by_reference)				\
	{							\
	  GEN_XCALLf(*(int *)proc_name[proc]);			\
	}							\
      break;							\
    case CHR:							\
      if (call_by_value)					\
	{							\
	  GEN_XCALLcv(*(int *)proc_name[proc]);			\
	}							\
      else if (call_by_reference)				\
	{							\
	  GEN_XCALLc(*(int *)proc_name[proc]);			\
	}							\
      break;							\
    case SIGNED_AC | CHR:					\
      if (call_by_value)					\
	{							\
	  GEN_XCALLscv(*(int *)proc_name[proc]);		\
	}							\
      else if (call_by_reference)				\
	{							\
	  GEN_XCALLsc(*(int *)proc_name[proc]);			\
	}							\
      break;							\
    case UNSIGNED_AC | CHR:					\
      if (call_by_value)					\
	{							\
	  GEN_XCALLucv(*(int *)proc_name[proc]);		\
	}							\
      else if (call_by_reference)				\
	{							\
	  GEN_XCALLuc(*(int *)proc_name[proc]);			\
	}							\
      break;							\
    case VID:							\
      if (call_by_value)					\
	{							\
	  GEN_XCALLvv(*(int *)proc_name[proc]);			\
	}							\
      else if (call_by_reference)				\
	{							\
	  GEN_XCALLv(*(int *)proc_name[proc]);			\
	}							\
      break;							\
    default:							\
      fprintfx (stderr, "%s:%d:\n", __FILE__, __LINE__);	\
      error_message (5000); return (0);				\
    }

#define GEN_RET ((struct OPERAND_0_ma *)kodp)->major = RET;\
kodp +=  sizeof (struct OPERAND_0_ma);

#define GEN_XCHG ((struct OPERAND_0_ma *)kodp)->major = XCHG;\
kodp += sizeof (struct OPERAND_0_ma);

#endif /* _GENINSTR_H */
