#include <stdio.h>
# define U_channel(x) x
# define NLSTATE_channel yy_channelprevious=YY_channelNEWLINE
# define BEGIN_channel yy_channelbgin = yy_channelsvec + 1 +
# define INITIAL_channel 0
# define YY_channelLERR yy_channelsvec
# define YY_channelSTATE (yy_channelestate-yy_channelsvec-1)
# define YY_channelOPTIM 1
# define YY_channelLMAX BUFSIZ
#ifndef __cplusplus
# define output_channel(c) (void)putc(c,yy_channelout)
#else
# define lex_output_channel(c) (void)putc(c,yy_channelout)
#endif

#if defined(__cplusplus) || defined(__STDC__)

#if defined(__cplusplus) && defined(__EXTERN_C__)
extern "C" {
#endif
	int yy_channelback(int *, int);
	int yy_channelinput_channel(void);
	int yy_channellook(void);
	void yy_channeloutput_channel(int);
	int yy_channelracc(int);
	int yy_channelreject(void);
	void yy_channelunput_channel(int);
	int yy_channellex(void);
#ifdef YY_channelLEX_E
	void yy_channelwoutput_channel(wchar_t);
	wchar_t yy_channelwinput_channel(void);
#endif
#ifndef yy_channelless
	int yy_channelless(int);
#endif
#ifndef yy_channelwrap
	int yy_channelwrap(void);
#endif
#ifdef LEXDEBUG
	void allprint(char);
	void sprint(char *);
#endif
#if defined(__cplusplus) && defined(__EXTERN_C__)
}
#endif

#ifdef __cplusplus
extern "C" {
#endif
	void exit(int);
#ifdef __cplusplus
}
#endif

#endif
# define unput_channel(c) {yy_channeltchar= (c);if(yy_channeltchar=='\n')yy_channellineno--;*yy_channelsptr++=yy_channeltchar;}
# define yy_channelmore() (yy_channelmorfg=1)
#ifndef __cplusplus
# define input_channel() (((yy_channeltchar=yy_channelsptr>yy_channelsbuf?U_channel(*--yy_channelsptr):getc(yy_channelin))==10?(yy_channellineno++,yy_channeltchar):yy_channeltchar)==EOF?0:yy_channeltchar)
#else
# define lex_input_channel() (((yy_channeltchar=yy_channelsptr>yy_channelsbuf?U_channel(*--yy_channelsptr):getc(yy_channelin))==10?(yy_channellineno++,yy_channeltchar):yy_channeltchar)==EOF?0:yy_channeltchar)
#endif
#define ECHO_channel fprintf(yy_channelout, "%s",yy_channeltext)
# define REJECT_channel { nstr_channel = yy_channelreject(); goto yy_channelfussy;}
int yy_channelleng;
char yy_channeltext[YY_channelLMAX];
int yy_channelmorfg;
extern char *yy_channelsptr, yy_channelsbuf[];
int yy_channeltchar;
FILE *yy_channelin = {stdin}, *yy_channelout = {stdout};
extern int yy_channellineno;
struct yy_channelsvf { 
	struct yy_channelwork *yy_channelstoff;
	struct yy_channelsvf *yy_channelother;
	int *yy_channelstops;};
struct yy_channelsvf *yy_channelestate;
extern struct yy_channelsvf yy_channelsvec[], *yy_channelbgin;

# line 3 "channel.l"
/*
 * channel.l
 *
 * lex source for channels
 * used as compiler for graphical interface
 *
 */

#include<string.h>
#ifdef input_channel
#undef input_channel
#undef yy_channelwrap
#endif
#include"mystdio.h"
#include"buf.h"
#include"ch-parser.h"
#include"init_ch.c"

#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif
#endif

#if 0
extern char *callocx PROTO((unsigned, unsigned));
#endif
extern char *string PROTO((char *));
extern void error_message PROTO((int));
# define YY_channelNEWLINE 10
yy_channellex(){
int nstr_channel; extern int yy_channelprevious;
#ifdef __cplusplus
/* to avoid CC and lint complaining yy_channelfussy not being used ...*/
static int __lex_hack = 0;
if (__lex_hack) goto yy_channelfussy;
#endif
while((nstr_channel = yy_channellook()) >= 0)
yy_channelfussy: switch(nstr_channel){
case 0:
if(yy_channelwrap()) return(0); break;
case 1:

# line 43 "channel.l"
	{}
break;
case 2:

# line 44 "channel.l"
	{;}
break;
case 3:

# line 45 "channel.l"
	{return (FIELDS);
			/* 
			 * Number of records per channel
                         */}
break;
case 4:

# line 49 "channel.l"
   	{return (TYPE);
			/* 
			 * Channel type (graphic)
 			 * now implemented only graphic channel
			 */}
break;
case 5:

# line 54 "channel.l"
{return (PRINT_FORMAT);
			/* 
			 * point
			 * line
  			 */}
break;
case 6:

# line 59 "channel.l"
{return (DIRECTION);
			/*
			 * input_channel or output_channel
			 * graphic channel implemented only as output_channel channel
			 */}
break;
case 7:

# line 64 "channel.l"
{return (START_TIME);}
break;
case 8:

# line 65 "channel.l"
{return (DURATION_TIME);
			/* 
			 * Length of the window in number of records
			 */}
break;
case 9:

# line 69 "channel.l"
{return (W_RESOLUTION);
			/* 
			 * Size of the windows
			 */}
break;
case 10:

# line 73 "channel.l"
	{return (LOWER);
			/* 
			 * Lower bound of the window
			 */}
break;
case 11:

# line 77 "channel.l"
	{return (UPPER);
			/* 
			 * Upper bound of the window
			 */}
break;
case 12:

# line 81 "channel.l"
	{return (STYLE);
			/* 
			 * Line type 
			 * now implemented as color setting 
			 */}
break;
case 13:

# line 86 "channel.l"
{return (ON_LEAVE_WINDOW);
		 	/*
			 * Suspend output_channel tto the window temporarily
			 * To continue press any key
			 */}
break;
case 14:

# line 91 "channel.l"
{return (AUTOMATIC);
			/*
			 * automatic for duration_time and start_time
			 */}
break;
case 15:

# line 95 "channel.l"
{sscanf (yy_channeltext, "%d", &yy_channellval.myint); return (NUMBERI);
			/* 
			 * Integer number
			 */}
break;
case 16:

# line 99 "channel.l"
{sscanf (yy_channeltext, "%lf", &yy_channellval.mydouble); return (NUMBERD);
			/* 
			 * Double number
			 */}
break;
case 17:

# line 103 "channel.l"
{yy_channellval.mystring = string (yy_channeltext);
		 return(STRING);
			/*
			 * String
			 */}
break;
case 18:

# line 108 "channel.l"
	{return(*yy_channeltext);}
break;
case -1:
break;
default:
(void)fprintf(yy_channelout,"bad switch yy_channellook %d",nstr_channel);
} return(0); }
/* end of yy_channellex */
int yy_channelvstop[] = {
0,

18,
0,

1,
18,
0,

2,
0,

18,
0,

18,
0,

15,
17,
18,
0,

17,
18,
0,

17,
18,
0,

17,
18,
0,

17,
18,
0,

17,
18,
0,

17,
18,
0,

17,
18,
0,

17,
18,
0,

17,
18,
0,

17,
18,
0,

17,
18,
0,

15,
0,

16,
0,

16,
0,

15,
17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

16,
0,

16,
17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

16,
0,

16,
0,

16,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

4,
17,
0,

17,
0,

17,
0,

16,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

10,
17,
0,

17,
0,

17,
0,

12,
17,
0,

11,
17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

3,
17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

13,
17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

14,
17,
0,

6,
17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

7,
17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

17,
0,

5,
17,
0,

9,
17,
0,

8,
17,
0,
0};
# define YY_channelTYPE unsigned char
struct yy_channelwork { YY_channelTYPE verify, advance; } yy_channelcrank[] = {
0,0,	0,0,	1,3,	0,0,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	1,4,	1,5,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	0,0,	0,0,	
1,6,	0,0,	0,0,	1,7,	
6,20,	1,8,	6,21,	6,21,	
6,21,	6,21,	6,21,	6,21,	
6,21,	6,21,	6,21,	6,21,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	1,9,	0,0,	
0,0,	0,0,	1,9,	2,7,	
0,0,	0,0,	0,0,	0,0,	
21,23,	0,0,	22,39,	0,0,	
1,10,	7,22,	7,22,	7,22,	
7,22,	7,22,	7,22,	7,22,	
7,22,	7,22,	7,22,	27,44,	
0,0,	0,0,	0,0,	0,0,	
0,0,	0,0,	1,11,	21,38,	
0,0,	1,12,	0,0,	1,13,	
2,10,	19,37,	47,65,	62,76,	
64,78,	1,14,	22,39,	31,48,	
44,62,	1,15,	13,31,	33,50,	
1,16,	1,17,	1,18,	10,27,	
1,19,	14,32,	2,11,	12,29,	
15,33,	2,12,	11,28,	2,13,	
16,34,	17,35,	18,36,	21,38,	
28,45,	2,14,	29,46,	12,30,	
30,47,	2,15,	32,49,	35,53,	
2,16,	2,17,	2,18,	8,23,	
2,19,	8,24,	8,24,	8,24,	
8,24,	8,24,	8,24,	8,24,	
8,24,	8,24,	8,24,	36,54,	
37,55,	40,59,	45,63,	46,64,	
48,66,	49,67,	8,25,	8,25,	
8,25,	8,25,	8,26,	8,25,	
8,25,	8,25,	8,25,	8,25,	
8,25,	8,25,	8,25,	8,25,	
8,25,	8,25,	8,25,	8,25,	
8,25,	8,25,	8,25,	8,25,	
8,25,	8,25,	8,25,	8,25,	
50,68,	40,59,	51,69,	52,70,	
8,25,	53,71,	8,25,	8,25,	
8,25,	8,25,	8,26,	8,25,	
8,25,	8,25,	8,25,	8,25,	
8,25,	8,25,	8,25,	8,25,	
8,25,	8,25,	8,25,	8,25,	
8,25,	8,25,	8,25,	8,25,	
8,25,	8,25,	8,25,	8,25,	
9,25,	9,25,	9,25,	9,25,	
9,25,	9,25,	9,25,	9,25,	
9,25,	9,25,	54,72,	55,73,	
63,77,	65,79,	66,80,	67,81,	
68,82,	9,25,	9,25,	9,25,	
9,25,	9,25,	9,25,	9,25,	
9,25,	9,25,	9,25,	9,25,	
9,25,	9,25,	9,25,	9,25,	
9,25,	9,25,	9,25,	9,25,	
9,25,	9,25,	9,25,	9,25,	
9,25,	9,25,	9,25,	69,83,	
70,84,	72,85,	73,86,	9,25,	
76,87,	9,25,	9,25,	9,25,	
9,25,	9,25,	9,25,	9,25,	
9,25,	9,25,	9,25,	9,25,	
9,25,	9,25,	9,25,	9,25,	
9,25,	9,25,	9,25,	9,25,	
9,25,	9,25,	9,25,	9,25,	
9,25,	9,25,	9,25,	23,40,	
23,40,	23,40,	23,40,	23,40,	
23,40,	23,40,	23,40,	23,40,	
23,40,	34,51,	77,88,	78,89,	
79,90,	80,91,	82,92,	83,93,	
26,42,	86,94,	26,42,	87,95,	
23,41,	26,43,	26,43,	26,43,	
26,43,	26,43,	26,43,	26,43,	
26,43,	26,43,	26,43,	88,96,	
38,42,	34,52,	38,42,	89,97,	
90,98,	38,56,	38,56,	38,56,	
38,56,	38,56,	38,56,	38,56,	
38,56,	38,56,	38,56,	92,99,	
39,57,	93,100,	39,57,	94,101,	
23,41,	39,58,	39,58,	39,58,	
39,58,	39,58,	39,58,	39,58,	
39,58,	39,58,	39,58,	41,60,	
95,102,	41,60,	96,103,	97,104,	
41,61,	41,61,	41,61,	41,61,	
41,61,	41,61,	41,61,	41,61,	
41,61,	41,61,	42,56,	42,56,	
42,56,	42,56,	42,56,	42,56,	
42,56,	42,56,	42,56,	42,56,	
43,43,	43,43,	43,43,	43,43,	
43,43,	43,43,	43,43,	43,43,	
43,43,	43,43,	57,58,	57,58,	
57,58,	57,58,	57,58,	57,58,	
57,58,	57,58,	57,58,	57,58,	
59,74,	98,105,	59,74,	99,106,	
100,107,	59,75,	59,75,	59,75,	
59,75,	59,75,	59,75,	59,75,	
59,75,	59,75,	59,75,	60,61,	
60,61,	60,61,	60,61,	60,61,	
60,61,	60,61,	60,61,	60,61,	
60,61,	74,75,	74,75,	74,75,	
74,75,	74,75,	74,75,	74,75,	
74,75,	74,75,	74,75,	101,108,	
103,109,	104,110,	105,111,	106,112,	
107,113,	108,114,	111,115,	112,116,	
113,117,	114,118,	115,119,	116,120,	
118,121,	119,122,	120,123,	121,124,	
122,125,	0,0,	0,0,	0,0,	
0,0};
struct yy_channelsvf yy_channelsvec[] = {
0,	0,	0,
yy_channelcrank+-1,	0,		0,	
yy_channelcrank+-25,	yy_channelsvec+1,	0,	
yy_channelcrank+0,	0,		yy_channelvstop+1,
yy_channelcrank+0,	0,		yy_channelvstop+3,
yy_channelcrank+0,	0,		yy_channelvstop+6,
yy_channelcrank+2,	0,		yy_channelvstop+8,
yy_channelcrank+33,	0,		yy_channelvstop+10,
yy_channelcrank+97,	0,		yy_channelvstop+12,
yy_channelcrank+172,	0,		yy_channelvstop+16,
yy_channelcrank+9,	yy_channelsvec+9,	yy_channelvstop+19,
yy_channelcrank+9,	yy_channelsvec+9,	yy_channelvstop+22,
yy_channelcrank+18,	yy_channelsvec+9,	yy_channelvstop+25,
yy_channelcrank+9,	yy_channelsvec+9,	yy_channelvstop+28,
yy_channelcrank+10,	yy_channelsvec+9,	yy_channelvstop+31,
yy_channelcrank+10,	yy_channelsvec+9,	yy_channelvstop+34,
yy_channelcrank+12,	yy_channelsvec+9,	yy_channelvstop+37,
yy_channelcrank+8,	yy_channelsvec+9,	yy_channelvstop+40,
yy_channelcrank+18,	yy_channelsvec+9,	yy_channelvstop+43,
yy_channelcrank+10,	yy_channelsvec+9,	yy_channelvstop+46,
yy_channelcrank+0,	yy_channelsvec+7,	0,	
yy_channelcrank+30,	yy_channelsvec+6,	yy_channelvstop+49,
yy_channelcrank+9,	yy_channelsvec+7,	yy_channelvstop+51,
yy_channelcrank+247,	0,		yy_channelvstop+53,
yy_channelcrank+0,	yy_channelsvec+8,	yy_channelvstop+55,
yy_channelcrank+0,	yy_channelsvec+9,	yy_channelvstop+58,
yy_channelcrank+269,	yy_channelsvec+9,	yy_channelvstop+60,
yy_channelcrank+15,	yy_channelsvec+9,	yy_channelvstop+62,
yy_channelcrank+16,	yy_channelsvec+9,	yy_channelvstop+64,
yy_channelcrank+20,	yy_channelsvec+9,	yy_channelvstop+66,
yy_channelcrank+22,	yy_channelsvec+9,	yy_channelvstop+68,
yy_channelcrank+10,	yy_channelsvec+9,	yy_channelvstop+70,
yy_channelcrank+19,	yy_channelsvec+9,	yy_channelvstop+72,
yy_channelcrank+10,	yy_channelsvec+9,	yy_channelvstop+74,
yy_channelcrank+208,	yy_channelsvec+9,	yy_channelvstop+76,
yy_channelcrank+27,	yy_channelsvec+9,	yy_channelvstop+78,
yy_channelcrank+43,	yy_channelsvec+9,	yy_channelvstop+80,
yy_channelcrank+42,	yy_channelsvec+9,	yy_channelvstop+82,
yy_channelcrank+285,	0,		0,	
yy_channelcrank+301,	0,		0,	
yy_channelcrank+88,	yy_channelsvec+23,	yy_channelvstop+84,
yy_channelcrank+316,	0,		0,	
yy_channelcrank+326,	0,		0,	
yy_channelcrank+336,	yy_channelsvec+9,	yy_channelvstop+86,
yy_channelcrank+11,	yy_channelsvec+9,	yy_channelvstop+89,
yy_channelcrank+47,	yy_channelsvec+9,	yy_channelvstop+91,
yy_channelcrank+58,	yy_channelsvec+9,	yy_channelvstop+93,
yy_channelcrank+9,	yy_channelsvec+9,	yy_channelvstop+95,
yy_channelcrank+52,	yy_channelsvec+9,	yy_channelvstop+97,
yy_channelcrank+60,	yy_channelsvec+9,	yy_channelvstop+99,
yy_channelcrank+78,	yy_channelsvec+9,	yy_channelvstop+101,
yy_channelcrank+76,	yy_channelsvec+9,	yy_channelvstop+103,
yy_channelcrank+83,	yy_channelsvec+9,	yy_channelvstop+105,
yy_channelcrank+92,	yy_channelsvec+9,	yy_channelvstop+107,
yy_channelcrank+129,	yy_channelsvec+9,	yy_channelvstop+109,
yy_channelcrank+130,	yy_channelsvec+9,	yy_channelvstop+111,
yy_channelcrank+0,	yy_channelsvec+42,	yy_channelvstop+113,
yy_channelcrank+346,	0,		0,	
yy_channelcrank+0,	yy_channelsvec+57,	yy_channelvstop+115,
yy_channelcrank+361,	0,		0,	
yy_channelcrank+371,	0,		0,	
yy_channelcrank+0,	yy_channelsvec+60,	yy_channelvstop+117,
yy_channelcrank+10,	yy_channelsvec+9,	yy_channelvstop+119,
yy_channelcrank+123,	yy_channelsvec+9,	yy_channelvstop+121,
yy_channelcrank+9,	yy_channelsvec+9,	yy_channelvstop+123,
yy_channelcrank+117,	yy_channelsvec+9,	yy_channelvstop+125,
yy_channelcrank+134,	yy_channelsvec+9,	yy_channelvstop+127,
yy_channelcrank+121,	yy_channelsvec+9,	yy_channelvstop+129,
yy_channelcrank+120,	yy_channelsvec+9,	yy_channelvstop+131,
yy_channelcrank+147,	yy_channelsvec+9,	yy_channelvstop+133,
yy_channelcrank+163,	yy_channelsvec+9,	yy_channelvstop+135,
yy_channelcrank+0,	yy_channelsvec+9,	yy_channelvstop+137,
yy_channelcrank+151,	yy_channelsvec+9,	yy_channelvstop+140,
yy_channelcrank+151,	yy_channelsvec+9,	yy_channelvstop+142,
yy_channelcrank+381,	0,		0,	
yy_channelcrank+0,	yy_channelsvec+74,	yy_channelvstop+144,
yy_channelcrank+150,	yy_channelsvec+9,	yy_channelvstop+146,
yy_channelcrank+209,	yy_channelsvec+9,	yy_channelvstop+148,
yy_channelcrank+191,	yy_channelsvec+9,	yy_channelvstop+150,
yy_channelcrank+203,	yy_channelsvec+9,	yy_channelvstop+152,
yy_channelcrank+194,	yy_channelsvec+9,	yy_channelvstop+154,
yy_channelcrank+0,	yy_channelsvec+9,	yy_channelvstop+156,
yy_channelcrank+215,	yy_channelsvec+9,	yy_channelvstop+159,
yy_channelcrank+216,	yy_channelsvec+9,	yy_channelvstop+161,
yy_channelcrank+0,	yy_channelsvec+9,	yy_channelvstop+163,
yy_channelcrank+0,	yy_channelsvec+9,	yy_channelvstop+166,
yy_channelcrank+202,	yy_channelsvec+9,	yy_channelvstop+169,
yy_channelcrank+214,	yy_channelsvec+9,	yy_channelvstop+171,
yy_channelcrank+211,	yy_channelsvec+9,	yy_channelvstop+173,
yy_channelcrank+226,	yy_channelsvec+9,	yy_channelvstop+175,
yy_channelcrank+221,	yy_channelsvec+9,	yy_channelvstop+177,
yy_channelcrank+0,	yy_channelsvec+9,	yy_channelvstop+179,
yy_channelcrank+241,	yy_channelsvec+9,	yy_channelvstop+182,
yy_channelcrank+229,	yy_channelsvec+9,	yy_channelvstop+184,
yy_channelcrank+239,	yy_channelsvec+9,	yy_channelvstop+186,
yy_channelcrank+273,	yy_channelsvec+9,	yy_channelvstop+188,
yy_channelcrank+257,	yy_channelsvec+9,	yy_channelvstop+190,
yy_channelcrank+252,	yy_channelsvec+9,	yy_channelvstop+192,
yy_channelcrank+295,	yy_channelsvec+9,	yy_channelvstop+194,
yy_channelcrank+296,	yy_channelsvec+9,	yy_channelvstop+196,
yy_channelcrank+303,	yy_channelsvec+9,	yy_channelvstop+198,
yy_channelcrank+322,	yy_channelsvec+9,	yy_channelvstop+200,
yy_channelcrank+0,	yy_channelsvec+9,	yy_channelvstop+202,
yy_channelcrank+341,	yy_channelsvec+9,	yy_channelvstop+205,
yy_channelcrank+331,	yy_channelsvec+9,	yy_channelvstop+207,
yy_channelcrank+347,	yy_channelsvec+9,	yy_channelvstop+209,
yy_channelcrank+329,	yy_channelsvec+9,	yy_channelvstop+211,
yy_channelcrank+335,	yy_channelsvec+9,	yy_channelvstop+213,
yy_channelcrank+329,	yy_channelsvec+9,	yy_channelvstop+215,
yy_channelcrank+0,	yy_channelsvec+9,	yy_channelvstop+217,
yy_channelcrank+0,	yy_channelsvec+9,	yy_channelvstop+220,
yy_channelcrank+330,	yy_channelsvec+9,	yy_channelvstop+223,
yy_channelcrank+338,	yy_channelsvec+9,	yy_channelvstop+225,
yy_channelcrank+347,	yy_channelsvec+9,	yy_channelvstop+227,
yy_channelcrank+344,	yy_channelsvec+9,	yy_channelvstop+229,
yy_channelcrank+345,	yy_channelsvec+9,	yy_channelvstop+231,
yy_channelcrank+354,	yy_channelsvec+9,	yy_channelvstop+233,
yy_channelcrank+0,	yy_channelsvec+9,	yy_channelvstop+235,
yy_channelcrank+341,	yy_channelsvec+9,	yy_channelvstop+238,
yy_channelcrank+344,	yy_channelsvec+9,	yy_channelvstop+240,
yy_channelcrank+338,	yy_channelsvec+9,	yy_channelvstop+242,
yy_channelcrank+345,	yy_channelsvec+9,	yy_channelvstop+244,
yy_channelcrank+355,	yy_channelsvec+9,	yy_channelvstop+246,
yy_channelcrank+0,	yy_channelsvec+9,	yy_channelvstop+248,
yy_channelcrank+0,	yy_channelsvec+9,	yy_channelvstop+251,
yy_channelcrank+0,	yy_channelsvec+9,	yy_channelvstop+254,
0,	0,	0};
struct yy_channelwork *yy_channeltop = yy_channelcrank+456;
struct yy_channelsvf *yy_channelbgin = yy_channelsvec+1;
char yy_channelmatch[] = {
  0,   1,   1,   1,   1,   1,   1,   1, 
  1,   9,  10,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  9,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,  43,   1,  43,   1,   1, 
 48,  48,  48,  48,  48,  48,  48,  48, 
 48,  48,   1,   1,   1,   1,   1,   1, 
  1,  65,  65,  65,  65,  69,  65,  65, 
 65,  65,  65,  65,  65,  65,  65,  65, 
 65,  65,  65,  65,  65,  65,  65,  65, 
 65,  65,  65,   1,   1,   1,   1,  65, 
  1,  65,  65,  65,  65,  69,  65,  65, 
 65,  65,  65,  65,  65,  65,  65,  65, 
 65,  65,  65,  65,  65,  65,  65,  65, 
 65,  65,  65,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
  1,   1,   1,   1,   1,   1,   1,   1, 
0};
char yy_channelextra[] = {
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0};
/*	Copyright (c) 1989 AT&T	*/
/*	  All Rights Reserved  	*/

/*	THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF AT&T	*/
/*	The copyright notice above does not evidence any   	*/
/*	actual or intended publication of such source code.	*/

#pragma ident	"@(#)ncform	6.8	95/02/11 SMI"

int yy_channellineno =1;
# define YY_channelU_channel(x) x
# define NLSTATE_channel yy_channelprevious=YY_channelNEWLINE
struct yy_channelsvf *yy_channellstate [YY_channelLMAX], **yy_channellsp, **yy_channelolsp;
char yy_channelsbuf[YY_channelLMAX];
char *yy_channelsptr = yy_channelsbuf;
int *yy_channelfnd;
extern struct yy_channelsvf *yy_channelestate;
int yy_channelprevious = YY_channelNEWLINE;
#if defined(__cplusplus) || defined(__STDC__)
int yy_channellook(void)
#else
yy_channellook()
#endif
{
	register struct yy_channelsvf *yy_channelstate, **lsp;
	register struct yy_channelwork *yy_channelt;
	struct yy_channelsvf *yy_channelz;
	int yy_channelch, yy_channelfirst;
	struct yy_channelwork *yy_channelr;
# ifdef LEXDEBUG
	int debug;
# endif
	char *yy_channellastch;
	/* start off machines */
# ifdef LEXDEBUG
	debug = 0;
# endif
	yy_channelfirst=1;
	if (!yy_channelmorfg)
		yy_channellastch = yy_channeltext;
	else {
		yy_channelmorfg=0;
		yy_channellastch = yy_channeltext+yy_channelleng;
		}
	for(;;){
		lsp = yy_channellstate;
		yy_channelestate = yy_channelstate = yy_channelbgin;
		if (yy_channelprevious==YY_channelNEWLINE) yy_channelstate++;
		for (;;){
# ifdef LEXDEBUG
			if(debug)fprintf(yy_channelout,"state %d\n",yy_channelstate-yy_channelsvec-1);
# endif
			yy_channelt = yy_channelstate->yy_channelstoff;
			if(yy_channelt == yy_channelcrank && !yy_channelfirst){  /* may not be any transitions */
				yy_channelz = yy_channelstate->yy_channelother;
				if(yy_channelz == 0)break;
				if(yy_channelz->yy_channelstoff == yy_channelcrank)break;
				}
#ifndef __cplusplus
			*yy_channellastch++ = yy_channelch = input_channel();
#else
			*yy_channellastch++ = yy_channelch = lex_input_channel();
#endif
			if(yy_channellastch > &yy_channeltext[YY_channelLMAX]) {
				fprintf(yy_channelout,"Input string too long, limit %d\n",YY_channelLMAX);
				exit(1);
			}
			yy_channelfirst=0;
		tryagain:
# ifdef LEXDEBUG
			if(debug){
				fprintf(yy_channelout,"char ");
				allprint(yy_channelch);
				putchar('\n');
				}
# endif
			yy_channelr = yy_channelt;
			if ( (int)yy_channelt > (int)yy_channelcrank){
				yy_channelt = yy_channelr + yy_channelch;
				if (yy_channelt <= yy_channeltop && yy_channelt->verify+yy_channelsvec == yy_channelstate){
					if(yy_channelt->advance+yy_channelsvec == YY_channelLERR)	/* error transitions */
						{unput_channel(*--yy_channellastch);break;}
					*lsp++ = yy_channelstate = yy_channelt->advance+yy_channelsvec;
					if(lsp > &yy_channellstate[YY_channelLMAX]) {
						fprintf(yy_channelout,"Input string too long, limit %d\n",YY_channelLMAX);
						exit(1);
					}
					goto contin;
					}
				}
# ifdef YY_channelOPTIM
			else if((int)yy_channelt < (int)yy_channelcrank) {		/* r < yy_channelcrank */
				yy_channelt = yy_channelr = yy_channelcrank+(yy_channelcrank-yy_channelt);
# ifdef LEXDEBUG
				if(debug)fprintf(yy_channelout,"compressed state\n");
# endif
				yy_channelt = yy_channelt + yy_channelch;
				if(yy_channelt <= yy_channeltop && yy_channelt->verify+yy_channelsvec == yy_channelstate){
					if(yy_channelt->advance+yy_channelsvec == YY_channelLERR)	/* error transitions */
						{unput_channel(*--yy_channellastch);break;}
					*lsp++ = yy_channelstate = yy_channelt->advance+yy_channelsvec;
					if(lsp > &yy_channellstate[YY_channelLMAX]) {
						fprintf(yy_channelout,"Input string too long, limit %d\n",YY_channelLMAX);
						exit(1);
					}
					goto contin;
					}
				yy_channelt = yy_channelr + YY_channelU_channel(yy_channelmatch[yy_channelch]);
# ifdef LEXDEBUG
				if(debug){
					fprintf(yy_channelout,"try fall back character ");
					allprint(YY_channelU_channel(yy_channelmatch[yy_channelch]));
					putchar('\n');
					}
# endif
				if(yy_channelt <= yy_channeltop && yy_channelt->verify+yy_channelsvec == yy_channelstate){
					if(yy_channelt->advance+yy_channelsvec == YY_channelLERR)	/* error transition */
						{unput_channel(*--yy_channellastch);break;}
					*lsp++ = yy_channelstate = yy_channelt->advance+yy_channelsvec;
					if(lsp > &yy_channellstate[YY_channelLMAX]) {
						fprintf(yy_channelout,"Input string too long, limit %d\n",YY_channelLMAX);
						exit(1);
					}
					goto contin;
					}
				}
			if ((yy_channelstate = yy_channelstate->yy_channelother) && (yy_channelt= yy_channelstate->yy_channelstoff) != yy_channelcrank){
# ifdef LEXDEBUG
				if(debug)fprintf(yy_channelout,"fall back to state %d\n",yy_channelstate-yy_channelsvec-1);
# endif
				goto tryagain;
				}
# endif
			else
				{unput_channel(*--yy_channellastch);break;}
		contin:
# ifdef LEXDEBUG
			if(debug){
				fprintf(yy_channelout,"state %d char ",yy_channelstate-yy_channelsvec-1);
				allprint(yy_channelch);
				putchar('\n');
				}
# endif
			;
			}
# ifdef LEXDEBUG
		if(debug){
			fprintf(yy_channelout,"stopped at %d with ",*(lsp-1)-yy_channelsvec-1);
			allprint(yy_channelch);
			putchar('\n');
			}
# endif
		while (lsp-- > yy_channellstate){
			*yy_channellastch-- = 0;
			if (*lsp != 0 && (yy_channelfnd= (*lsp)->yy_channelstops) && *yy_channelfnd > 0){
				yy_channelolsp = lsp;
				if(yy_channelextra[*yy_channelfnd]){		/* must backup */
					while(yy_channelback((*lsp)->yy_channelstops,-*yy_channelfnd) != 1 && lsp > yy_channellstate){
						lsp--;
						unput_channel(*yy_channellastch--);
						}
					}
				yy_channelprevious = YY_channelU_channel(*yy_channellastch);
				yy_channellsp = lsp;
				yy_channelleng = yy_channellastch-yy_channeltext+1;
				yy_channeltext[yy_channelleng] = 0;
# ifdef LEXDEBUG
				if(debug){
					fprintf(yy_channelout,"\nmatch ");
					sprint(yy_channeltext);
					fprintf(yy_channelout," action %d\n",*yy_channelfnd);
					}
# endif
				return(*yy_channelfnd++);
				}
			unput_channel(*yy_channellastch);
			}
		if (yy_channeltext[0] == 0  /* && feof(yy_channelin) */)
			{
			yy_channelsptr=yy_channelsbuf;
			return(0);
			}
#ifndef __cplusplus
		yy_channelprevious = yy_channeltext[0] = input_channel();
		if (yy_channelprevious>0)
			output_channel(yy_channelprevious);
#else
		yy_channelprevious = yy_channeltext[0] = lex_input_channel();
		if (yy_channelprevious>0)
			lex_output_channel(yy_channelprevious);
#endif
		yy_channellastch=yy_channeltext;
# ifdef LEXDEBUG
		if(debug)putchar('\n');
# endif
		}
	}
#if defined(__cplusplus) || defined(__STDC__)
int yy_channelback(int *p, int m)
#else
yy_channelback(p, m)
	int *p;
#endif
{
	if (p==0) return(0);
	while (*p) {
		if (*p++ == m)
			return(1);
	}
	return(0);
}
	/* the following are only used in the lex library */
#if defined(__cplusplus) || defined(__STDC__)
int yy_channelinput_channel(void)
#else
yy_channelinput_channel()
#endif
{
#ifndef __cplusplus
	return(input_channel());
#else
	return(lex_input_channel());
#endif
	}
#if defined(__cplusplus) || defined(__STDC__)
void yy_channeloutput_channel(int c)
#else
yy_channeloutput_channel(c)
  int c; 
#endif
{
#ifndef __cplusplus
	output_channel(c);
#else
	lex_output_channel(c);
#endif
	}
#if defined(__cplusplus) || defined(__STDC__)
void yy_channelunput_channel(int c)
#else
yy_channelunput_channel(c)
   int c; 
#endif
{
	unput_channel(c);
	}
