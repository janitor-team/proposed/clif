/*
 * sigig.c
 *
 * Ignores interrupts in a fork-ed process.
 * It is used from rw.c file.
 */

#include <signal.h>

#ifndef PROTO
#if defined (USE_PROTOTYPES) ? USE_PROTOTYPES : defined(__STDC__)
#define PROTO(ARGS) ARGS
#else
#define PROTO(ARGS) ()
#endif 
#endif

void mysigignore PROTO((void));

void 
mysigignore ()
{
  sigignore (SIGINT);
}
